package qianbao.com.guolvv1.activity.login;

import android.text.Editable;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import qianbao.com.guolvv1.R;
import qianbao.com.guolvv1.activity.forget_login_pwd.ForgetLoginPwdActivity;
import qianbao.com.guolvv1.activity.idcard_authentication.IdcardAuthenticationActivity;
import qianbao.com.guolvv1.base.BaseActivity;
import qianbao.com.guolvv1.utils.ActivityJumpUtils;
import qianbao.com.guolvv1.widget.views.GraphCodeView;

/**
 * Created by liuyong
 * Data: 2018/5/28
 * Github:https://github.com/MrAllRight
 */

public class LoginActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.create_account)
    TextView tvCreateAccount;
    @BindView(R.id.login_edt_username)
    EditText etUserName;//用户名
    @BindView(R.id.login_edt_password)
    EditText etUserPass;//密码
    @BindView(R.id.login_edt_check_password)
    EditText etVerficationCode;//图形验证码
    @BindView(R.id.rl_code_check)
    RelativeLayout rlVerficationCode;//图形验证码布局
    @BindView(R.id.bt_login)
    Button btLogin;//登陆按钮

    @BindView(R.id.login_cbx_password)
    CheckBox cbxSeePassWord;//密码小眼睛

    @BindView(R.id.codeview)
    GraphCodeView codeView;//图形验证码
    @BindView(R.id.login_img_delete)
    ImageView imDelete;//删除

    @Override
    protected int setContentLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initConfigs() {
        cbxSeePassWord.setOnCheckedChangeListener(this);
        showPassWordVisible(false);
    }

    private void showPassWordVisible(boolean canSee) {
        if (canSee) {
            etUserPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            etUserPass.setSelection(etUserPass.getText().length());
        } else {
            etUserPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            etUserPass.setSelection(etUserPass.getText().length());
        }
    }

    /**
     * 进去注册
     */
    @OnClick(R.id.create_account)
    void onCreateAccount() {
        ActivityJumpUtils.commonNext(LoginActivity.this, IdcardAuthenticationActivity.class, false);
    }

    /**
     * 忘记登陆
     */
    @OnClick(R.id.text_regist)
    void forgetLoginPass() {
        ActivityJumpUtils.commonNext(LoginActivity.this, ForgetLoginPwdActivity.class, false);
    }

    /**
     * 登陆
     */
    @OnClick(R.id.bt_login)
    void login() {
        String userName = etUserName.getText().toString();
        String userPass = etUserPass.getText().toString();
        String graphCode = null;
        if (View.VISIBLE == rlVerficationCode.getVisibility()) {
            graphCode = etVerficationCode.getText().toString();
//                    presenter.loadUserInfo(userName, userPass, graphCode, true);
        } else {
//                    presenter.loadUserInfo(userName, userPass, graphCode,false);
        }
    }

    /**
     * 全清手机号
     */
    @OnClick(R.id.login_img_delete)
    void clearPhoneNum() {
        etUserName.setText("");
        etUserName.requestFocus();
    }

    /**
     * 图形验证码点击
     */
    @OnClick(R.id.codeview)
    void getGraphCode() {
//        presenter.getGraphCode(etUserName.getText().toString());
    }


    /**
     * 小眼睛显示密码明文
     */
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        showPassWordVisible(isChecked);
    }

    /**
     * 监听输入框，手机号的显示删除全部图片。按钮是否可点击
     */
    @OnTextChanged(value = {R.id.login_edt_username, R.id.login_edt_password}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void listenEditText(Editable chars) {
        if (etUserName.getText().toString().length() > 0) {
            imDelete.setVisibility(View.VISIBLE);
        } else {
            imDelete.setVisibility(View.GONE);
        }
        setLoginButtonEnable();
    }

    /**
     * 设置按钮是否可以点击
     */
    public void setLoginButtonEnable() {
        Boolean clickable = !(TextUtils.isEmpty(etUserName.getText().toString()) || TextUtils.isEmpty(etUserPass.getText().toString()));
        btLogin.setEnabled(clickable);
        if (clickable) {
            btLogin.setTextColor(getResources().getColor(R.color.white));
            btLogin.setBackgroundColor(getResources().getColor(R.color.color_blue_btn_release));
        } else {
            btLogin.setTextColor(getResources().getColor(R.color.white_alpha));
            btLogin.setBackgroundColor(getResources().getColor(R.color.colorffb6b5));
        }
    }

    public void showGraphCode(String code) {
        rlVerficationCode.setVisibility(View.VISIBLE);
        codeView.setText(code);
    }
}
