package com.qianbao.bankplatformmvp.widget.views.pull_push_refresh_view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;

import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;

/**
 * @author YuliangZhang
 * @create 2017/8/26.
 * @description BaseFrame
 */

public class SupportHeadFootLayout extends TwinklingRefreshLayout {

    public PullPushHeaderRefreshView mPtrClassicHeader;//头部
    public PullPushLoadMoreView mPtrClassicFooter;//底部
    private boolean animDoing = false;//防止多次调用成功对钩动画
    private boolean canEndRefreshView = true;//防止多次调用成功(刷新接口过多够频繁时使用)

    public SupportHeadFootLayout(Context context) {
        super(context);
        initViews();
    }

    public SupportHeadFootLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    /**
     * 设置头部视图是否隐藏
     */
    public void setHeaderViewInvisible() {
        mPtrClassicHeader.setVisibility(INVISIBLE);
    }

     /**
     * 设置底部视图是否隐藏
     */
    public void setFootViewNoMore(boolean isNoMore) {
        mPtrClassicFooter.setNoMore(isNoMore);
    }


    /**
     * 设置头部视图初始化
     */
    private void initViews() {
        //设置下拉头部视图
        mPtrClassicHeader = new PullPushHeaderRefreshView(getContext());
        setHeaderView(mPtrClassicHeader);
        //设置上拉底部视图
        mPtrClassicFooter = new PullPushLoadMoreView(getContext());
        setBottomView(mPtrClassicFooter);
    }

    /**
     * 刷新完成数据回来
     */
    public void endRefresh() {
        if (!canEndRefreshView) return;//还没开始刷新禁止调用结束
        if (animDoing) {return;}//动画进行中 returned
        animDoing = true;//动画启动
        mPtrClassicHeader.refreshSucc();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finishRefreshing();
                animDoing = false;//动画结束
                canEndRefreshView= false;//不可以再次调用结束（只有下拉才打开，防止抖动）
            }
        }, PullPushHeaderRefreshView.nikeDuration);
    }

     /**
     * 刷新完成数据回来
     */
    public void endLodeMore() {
       finishLoadmore();
    }


    /**
     * 开始刷新
     */
    @Override
    public void onRefresh(TwinklingRefreshLayout refreshLayout) {
        super.onRefresh(refreshLayout);
        canEndRefreshView= true;//下拉刷新时打开可以结束开关
    }

}