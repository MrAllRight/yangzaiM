package qianbao.com.guolvv1.http.subscriber;

import com.orhanobut.logger.Logger;

import qianbao.com.guolvv1.base.IBaseView;
import qianbao.com.guolvv1.config.Config;
import qianbao.com.guolvv1.entity.CommonFatherEntity;
import qianbao.com.guolvv1.http.FinalCallBack;


/**
 * @author YuliangZhang
 * @create 2017/5/23.
 * @description 处理页面加载弹窗，将解密后的结果或异常反馈给presenter层处理,泛型传入为CommonFatherEntity
 */

public class DealCommonFatherEntity<T>  extends BaseSubscriber<T> {
    FinalCallBack commonResultCallBack;
    private String logTag = "baseNetSubscriber";
    private IBaseView view;
    /**
     * @param commonResultCallBack 结果回调
     * @param view                 页面
     * @param isShowProgress       是否加载动画弹窗
     */
    public DealCommonFatherEntity(FinalCallBack commonResultCallBack, IBaseView view, Boolean isShowProgress) {
        super(commonResultCallBack, view, isShowProgress);
        this.commonResultCallBack=commonResultCallBack;
        this.view=view;
    }

    @Override
    protected <T1> void onNextDataDeal(T1 commonFatherEntity1) {
        CommonFatherEntity commonFatherEntity = (CommonFatherEntity) commonFatherEntity1;
        String state = commonFatherEntity.getStatus();
        String message = commonFatherEntity.getMessage();
        String msgForUser = commonFatherEntity.getInfo();
        if (msgForUser == null) {//用户信息没配置
            msgForUser = message;
        }

        switch (commonFatherEntity.getStatus()){
            case Config.CODE_SUCCESS:
                commonResultCallBack.onSuccess(commonFatherEntity.getStatus(),commonFatherEntity.getResult());
                Logger.t(logTag).i("成功返回presenter层");
                break;

            case Config.CODE_TOKEN_ERROR:
            case Config.CODE_TOKEN_NULL:
                if (view != null) {
                    view.showTokenErrorDialog(msgForUser);
                    Logger.t(logTag).i("不返回presenter层（token失效统一弹窗处理）");
                } else {
                    commonResultCallBack.onFailure(state, msgForUser);
                    Logger.t(logTag).i("失败返回presenter层（token失效）");
                }
                break;

            default:
                commonResultCallBack.onFailure(state, msgForUser);
                Logger.t(logTag).i("失败返回presenter层");
                break;
        }
    }

}