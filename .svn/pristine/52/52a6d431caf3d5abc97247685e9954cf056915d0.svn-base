package qianbao.com.guolvv1.utils;

import android.util.Base64;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import qianbao.com.guolvv1.BuildConfig;
import qianbao.com.guolvv1.config.Config;

/**
 * @author YuliangZhang
 * @create 2018/3/22.
 * @description Zhonglv_bank_v2.0
 */

public class RsaEncryptionUtils {

    private static final String RSA = "RSA";// 非对称加密密钥算法
    private static final String ECB_PKCS1_PADDING = "RSA/ECB/PKCS1Padding";//加密填充方式
    private static final int DEFAULT_KEY_SIZE = 2048;//秘钥默认长度

    private static final int MAX_DECRYPT_BLOCK = 128; //RSA最大解密密文大小
    private static final int MAX_ENCRYPT_BLOCK = 117; //RSA最大加密密文大小


    /**
     * 用公钥分段加密（每次加密的字节数，不能超过密钥的长度值减去11）
     * @param data      需加密数据的byte数据
     * @param publicKey 公钥
     * @return 加密后的byte型数据
     */
    public static byte[] encryptData(byte[] data, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        // 编码前设定编码方式及密钥
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        // 传入编码数据并返回编码结果
        int inputLen = data.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段加密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(data, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_ENCRYPT_BLOCK;
        }
        byte[] encryptedData = out.toByteArray();
        out.close();
        return encryptedData;
    }



    /**
     * 用私钥分段解密
     * @param encryptedData 经过encryptedData()加密返回的byte数据
     * @param privateKey    私钥
     */
    public static byte[] decryptData(byte[] encryptedData, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        int inputLen = encryptedData.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();
        return decryptedData;
    }



    /**
     * 从字符串中加载公钥
     * @param publicKeyStr 公钥数据字符串
     * @throws Exception 加载公钥时产生的异常
     */
    public static PublicKey loadPublicKey(String publicKeyStr) throws Exception {
        try {
            byte[] buffer = Base64.decode(publicKeyStr, Base64.DEFAULT);
            KeyFactory keyFactory = KeyFactory.getInstance(RSA);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            return (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("公钥非法");
        } catch (NullPointerException e) {
            throw new Exception("公钥数据为空");
        }
    }



    /**
     * 从字符串中加载私钥<br>
     * 加载时使用的是PKCS8EncodedKeySpec（PKCS#8编码的Key指令）。
     * @param privateKeyStr
     * @return
     * @throws Exception
     */
    public static PrivateKey loadPrivateKey(String privateKeyStr) throws Exception {
        try {
            byte[] buffer = Base64.decode(privateKeyStr, Base64.DEFAULT);
            // X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
            KeyFactory keyFactory = KeyFactory.getInstance(RSA);
            return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("私钥非法");
        } catch (NullPointerException e) {
            throw new Exception("私钥数据为空");
        }
    }

    /**
     * ----------------------------以下为业务逻辑定制部分---------------------------------------
     */


    /**
     * 通过加解密类型判断
     */
    public static String byDetryptionKey(String type, String data) throws Exception {
        String resulte = "";
        switch (type) {
            case Config.ENCRY_TYPE4://rsa加密
                // 从字符串中得到私钥
                PrivateKey privateKey = loadPrivateKey(BuildConfig.KEY_LOGIN_DECODE);
                // 因为RSA加密后的内容经Base64再加密转换了一下，所以先Base64解密回来再给RSA解密
                byte[] decryptByte =decryptData(Base64.decode(data, Base64.DEFAULT), privateKey);
                resulte = new String(decryptByte);
                break;
        }
        return resulte;
    }

    /**
     * 通过加解密类型判断
     */
    public static String byEntryptionKey(Map<String, Object> map, String type) {
        String resulte = "";
        switch (type) {
            case Config.ENCRY_TYPE4://rsa加密
                resulte = map2EncryptRsaString(map, BuildConfig.KEY_LOGIN_ENCRY);
                break;
        }
        return resulte;
    }


    /**
     * map转化为rsa加密字符串
     */
    private static String map2EncryptRsaString(Map<String, Object> map,String key) {
        JSONObject jsonObject = new JSONObject(map);
        byte[] content = null;
        try {
            PublicKey publicKey = loadPublicKey(BuildConfig.KEY_LOGIN_ENCRY);
            content =encryptData( jsonObject.toString().getBytes(),publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(content, Base64.DEFAULT);
    }




    /**
     * 数据包装类，加content,有些接口需要传的参数包装一层“content”
     */
    public static String contentWarp(String map) {
        Map<String, String> m = new HashMap<String, String>();
        m.put("content", map);
        JSONObject json = new JSONObject(m);
        return json.toString();
    }







}
