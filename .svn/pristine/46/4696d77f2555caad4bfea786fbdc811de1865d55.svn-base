package qianbao.com.guolvv1.config;

/**
 * @author YuliangZhang
 * @create 2017/5/25.
 * @description 配置常量
 */

public class Config {


    /**
     * 多步骤页面常量（如注册，忘记密码）
     */
    public static final int STEP_1 = 1;//第一步
    public static final int STEP_2 = 2;//第二步
    public static final int STEP_3 = 3;//第三步


    /**
     * 应用基本信息配置项
     */
    public static final String OCR_PLATFORM= "xlx";//OCR平台号
    public static final String PLATFORM_ZH = "zhonglvqianbao";//综合平台记号
    public static final String UNI_SOURCE = "bank/jzctb";//resthub需要
    public static final String SOURCE = "android";//android平台号
    public static final String PLATFORM_M = "bank";//平台号
    public static final String PLATFORM_B = "jzctb";//平台号
    public static final String PLATFORM = "jzctb";//平台
    public static final String ASSETS_DIR = "assets";//文件存储路径
    public static final String APP_ID = "wx702a88b3ddc49dd7";//微信支付id
    public static final String APP_WEBVIEW_CACAHE_DIRNAME = "/webcache";//webwiew缓存路径
    public static final long APP_EXIT_TIMER = 2000;//两次后退键退出时间间隔


    /**
     * 网络接口返回的状态码
     */
    public static final String CODE_OCR_SUCC = "55000048";//用户已ocr认证
    public static final String CODE_SUCCESS = "20000000";
    public static final String CODE_NEED_CODE = "4628";
    public static final String CODE_NEED_CODE1 = "4629";
    public static final String CODE_TOKEN_ERROR = "20001315";//TOKEN错误或TOKEN已过期，请重新登录
    public static final String CODE_TOKEN_NULL = "20001301";//TOKEN为空
    public static final String CODE_NET_ERROR = "10086";//网络断开，本地自定义

    /**
     * 加解密类型
     * */
    public static final String ENCRY_TYPE0 = "noEncry";//无需加解密
    public static final String ENCRY_TYPE1 = "encryType1";//3des加解密(艳超)
    public static final String ENCRY_TYPE2 = "encryType2";//3des加解密(世伟登录)
    public static final String ENCRY_TYPE3 = "encryType3";//3des加解密(世伟公共)
    public static final String ENCRY_TYPE4 = "encryType4";//3des加解密(世伟公共)




    /**
     * 网络数据结构类型（网络解析数据function）
     */
    public static final String COMMON_DATA = "common";//通用的数据格式（内容主要在result字段中）
    public static final String HOME_LIST_DATA = "homeList";//首页理财列表（内容主要在data字段中）
    public static final String OTHERS_DATA = "others";//其他数据格式

    /**
     * 短信验证码接口分辨不同业务类型
     */
    public static final String TYPE_LOGIN = "login";//登录
    public static final String TYPE_REGISTER = "registered";//注册
    public static final String TYPE_FORGET_LOGIN_VIEW = "forgetPassword";//忘记登陆密码
    public static final String TYPE_CHANGE_LOGIN_VIEW = "modifyPassWord";//修改登录密码
    public static final String TYPE_CHANGE_PAY_VIEW = "modifyTradePwd";//修改交易密码
    public static final String TYPE_FORGET_PAY_VIEW = "retTradePwd";//忘记交易密码
    public static final String TYPE_BAND_CARD = "saveBankCard";//绑卡
    public static final String RELEASE_BAND_CARD = "unbindBank";//解绑卡
    public static final String TYPE_BORROW = "iWantToBorrow ";//我要借款

    /**
     * webview页面配置
     */
    public static final String NEED_OPEN_URL = "url";//传值key h5需要打开的页面

    public static final String BRIDGE_LOGIN = "jzctb_login";//登录的回调
    public static final String BRIDGE_LOGIN_KEY_INFO = "info";//登陆返回信息

    public static final String BRIDGE_SHARE = "jzctb_share";//分享的回调
    public static final String BRIDGE_SHARE_BACK= "jzctb_share_back";//调用h5桥
    public static final String WXHY= "wxhy";//分享到微信好友
    public static final String WXPYQ= "wxpyq";//分享到朋友圈
    public static final String SHARE_DEAF= "DEFAULT_SHARE";//默认（以上为空时）

    public static final String DETAIL_URL= "detailUrl";//点开url
    public static final String SHARE_TITLE= "title";//标题
    public static final String SHARE_IMAGE= "imageUrl";//图片地址
    public static final String SHARE_DESC= "desc";//描述
    public static final String SHARE_WHERE= "where";//分享到哪里

    public static final String BRIDGE_RIGHT_SEAT= "jzctb_right_seat_setting";//头部最右侧文字设定
    public static final String TITLE_TEXT= "titleText";//最右侧站位为文字内容
    public static final String ICON= "icon";//最右侧为图标时
    public static final String CLICK_URL= "needOpenUrl";//为文字点开跳转路径
    public static final String ICON_SHARE= "share";//为图标时显示具体图分享

    public static final String BRIDGE_HEADER＿COLOR= "jzctb_title_colors";//头部颜色的回调
    public static final String BG_COLOR= "bgColor";//背景色
    public static final String TEXT_COLOR= "textColor";//文字颜色
    public static final String SPECIAL_COLOR= "#ffffff";//特殊颜色白色处理
    /**
     * 状态码
     */
    public static final String CODE_NEED_PIC_CODE = "55021651";//需要添加图形验证码
    public static final String CODE_LOGIN_ERROR = "55029651";
    public static final String CODE_SOME_VALUE_NULL = "55062651";//关键值为空
    public static final String CODE_PIC_CODE_TIMEOUT = "55026651";//图形验证码失效

    /**
     * 其他
     * */
    public static final String CODE_MSG = "CODE_MSG";//二维码扫描信息
    public static final String DETAIL_IMAG_URL = "DETAIL_IMAG_URL";//图片放大详情url
    public static final String DETAIL_IMAG_SHARE = "share";//共享元素动画，在布局中两个activity都需要指定 android:transitionName="share"

}
