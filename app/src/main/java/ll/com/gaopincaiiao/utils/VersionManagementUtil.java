package ll.com.gaopincaiiao.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.Arrays;

/**
 * @author YuliangZhang
 * @create 2017/3/22.
 * @description 版本号工具类
 */

public class VersionManagementUtil {
    private static final String TAG = "VersionManagementUtil";
    /**
     * 获取版本号
     *
     * @return 当前应用的版本号，默认是1.0.0
     */
    public static String getCurrentVersionName(Context mContext) {

        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "1.0.0";
        }
    }

    /**
     * 当前应用版本号
     *
     * @return
     */
    public static int getCurrentVersionCode(Context mContext) {
        try {
            PackageManager packageManager = mContext.getPackageManager();
            String packageName = mContext.getPackageName();
            PackageInfo info = packageManager.getPackageInfo(packageName, 0);
            return info.versionCode;
        } catch (Exception e) {
        }
        return 1;
    }


    /**
     * 是否需要更新判断
     *
     * @return ture需要
     */
    public static boolean isNeedUpdate(String versionServer, Context mContext) {
        String versionLocal=getCurrentVersionName(mContext);
        if (VersionComparison(versionServer, versionLocal) > 0) {//需要更新
            return true;
        } else {//不需要更新
            return false;
        }


    }

    /**
     * @param versionServer 服务器版本号
     * @param versionLocal 本地版本号
     * @return 服务器版本号>本地版本号return 1, 两个版本号相等return 0, 服务器版本号<本地版本号return-1
     */
    private static int VersionComparison(String versionServer, String versionLocal) {
        String version1 = versionServer;
        String version2 = versionLocal;
        if (version1 == null || version1.length() == 0 || version2 == null || version2.length() == 0)
            throw new IllegalArgumentException("Invalid parameter!");

        int index1 = 0;
        int index2 = 0;
        while (index1 < version1.length() && index2 < version2.length()) {
            int[] number1 = getValue(version1, index1);
            Log.i(TAG, " ===== 服务器 ====" + Arrays.toString(number1));
            int[] number2 = getValue(version2, index2);
            Log.i(TAG, " ===== 本地 ====" + Arrays.toString(number2));

            if (number1[0] < number2[0]) {
                Log.i(TAG, " ===== 服务器[0] ====" + number1[0]);
                Log.i(TAG, " ===== 本地[0] ====" + number2[0]);
                return -1;
            } else if (number1[0] > number2[0]) {
                Log.i(TAG, " ===== 服务器[0] ====" + number1[0]);
                Log.i(TAG, " ===== 本地[0] ====" + number2[0]);
                return 1;
            } else {
                index1 = number1[1] + 1;
                index2 = number2[1] + 1;
            }
        }
        if (index1 == version1.length() && index2 == version2.length()) return 0;
        if (index1 < version1.length()) return 1;
        else return -1;
    }

    /**
     * @param version 版本号
     * @param index   the starting point
     * @return the number between two dots, and the index of the dot
     */
    private static int[] getValue(String version, int index) {
        int[] value_index = new int[2];
        StringBuilder sb = new StringBuilder();
        while (index < version.length() && version.charAt(index) != '.') {
            sb.append(version.charAt(index));
            index++;
        }
        value_index[0] = Integer.parseInt(sb.toString());
        value_index[1] = index;

        return value_index;
    }


}
