package ll.com.gaopincaiiao.utils;

import android.net.Uri;
import android.text.TextUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ll.com.gaopincaiiao.App;

/**
 * @author YuliangZhang
 * @create 2017/5/24.
 * @description BankPlatformMvp
 */

public class StringUtils {
    /**
     * 将金额转换为万，并保留2位小数，不四舍五入
     */
    public static String getTwoDecimal(String price) {
        try {
            BigDecimal bigDecimal = new BigDecimal(price);
            // 转换为万元（除以10000）
            BigDecimal bigDecimalWan = new BigDecimal("1000000");
            BigDecimal decimal = bigDecimal.divide(bigDecimalWan).setScale(2, BigDecimal.ROUND_DOWN);
            return decimal.toString();
        } catch (Exception e) {
            return "";
        }
    }


    /**
     * 每四位加个空格
     */
    public static String formatBankCardNum(String num) {
        return num.replaceAll(".{4}(?!$)", "$0 ");
    }

    /**
     * 清除空格
     */
    public static String clearSpace(String num) {
        String result = num.trim();
        return result.replaceAll("\\s*", "");
    }

    /**
     * 清除金钱的千分逗号
     */
    public static String clearComma(String num) {
        String result = num.trim();
        result.replaceAll("\\s*", "");
        return result.replaceAll(",", "");
    }

    /**
     * 手机号加 * 号
     */
    public static String mixPhone(String phone) {
        if ("".equals(phone) || phone.length() != 11) {
            phone = "";
        } else {
            phone = phone.substring(0, 3) + "****" + phone.substring(7);
        }
        return phone;
    }

    /**
     * 身份证号加 * 号
     */
    public static String mixIdCard(String idNum) {
        if ("".equals(idNum)) {
            idNum = "";
        } else {
            idNum = idNum.substring(0, 3) + "************" + idNum.substring(idNum.length() - 3);
        }
        return idNum;
    }

    /**
     * 银行卡号加 * 号
     */
    public static String mixBankCard(String idNum) {
        if ("".equals(idNum)) {
            idNum = "";
        } else {
//            idNum = idNum.substring(0, 4) + " **** **** **** " + idNum.substring(idNum.length() - 4);//留首尾
            idNum = " **** **** **** " + idNum.substring(idNum.length() - 4);//留尾
        }
        return idNum;
    }


    /**
     * 把String转化为double
     */
    public static double convertToDouble(String number, double defaultValue) {
        if (TextUtils.isEmpty(number)) {
            return defaultValue;
        }
        try {
            return Double.parseDouble(number);
        } catch (Exception e) {
            return defaultValue;
        }

    }

    /**
     * 把String分 转化为 string元
     */
    public static String fenToYuan(String number, String defaultValue) {
        if (TextUtils.isEmpty(number)) {
            return defaultValue;
        }
        try {
            String resultNum = "";
            BigDecimal bigDecimal = new BigDecimal(number);
            BigDecimal bigDecimal2 = bigDecimal.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_DOWN);//保2位小数
            resultNum = bigDecimal2.toString();
            return resultNum;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    /**
     * 把String分 转化为 string元
     */
    public static String fenToYuanNoDecimal(String number, String defaultValue) {
        if (TextUtils.isEmpty(number)) {
            return defaultValue;
        }
        try {
            String resultNum = "";
            BigDecimal bigDecimal = new BigDecimal(number);
            BigDecimal bigDecimal2 = bigDecimal.divide(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_DOWN);//保2位小数
            resultNum = bigDecimal2.toString();
            return resultNum;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    /**
     * 把String元 转化为 string分
     */
    public static String yuanToFen(String number, String defaultValue) {
        if (TextUtils.isEmpty(number)) {
            return defaultValue;
        }
        try {
            String resultNum = "";
            BigDecimal bigDecimal = new BigDecimal(number);
            BigDecimal bigDecimal2 = bigDecimal.multiply(new BigDecimal(100)).setScale(0);//取整数
            resultNum = bigDecimal2.toString();
            return resultNum;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    /**
     * 金钱千位分隔符,并且小数点后保留2位
     */
    public static String qianweifenge(String num) {
        try {
            if (!isMoney(num)) {
                return num;
            } else {
                double nums = convertToDouble(num, 0);
                DecimalFormat df = new DecimalFormat("#,##0.00");
                String ss = df.format(nums);
                return ss;
            }
        } catch (Exception e) {
            return "----";
        }
    }

    /**
     * 金钱千位分隔符,并且小数点后保留2位
     */
    public static String qianweifenge(double num) {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        String ss = df.format(num);
        return ss;
    }

    /**
     * url校验
     */
    public static String getUrlParam(String url, String name) {
        return Uri.parse(url).getQueryParameter(name);
    }

    /**
     * 获取规则uuid
     */
    public static String getUUID() {
        String s = UUID.randomUUID().toString();
        s = s.replace("-", "");
        s = s.toLowerCase();
        return s;
    }

    /**
     * 判断是否为手机号
     */
    public static boolean isMobileNO(String mobiles) {
        String regEx = "^1\\d{10}$";
        Pattern p = Pattern.compile(regEx);
        return p.matcher(mobiles).matches();
    }

    /**
     * 判断是否为中文
     */
    public static boolean isChinese(String input) {
        String regEx = "^[\u4e00-\u9fa5]+$";
        Pattern p = Pattern.compile(regEx);
        return p.matcher(input).matches();
    }

    /**
     * 判断是否为金钱
     */
    public static boolean isMoney(String money) {
        try {
            String regEx = "^([1-9])|([1-9][0-9]+)|([1-9][0-9]+.[0-9]{1,2})|([1-9].[0-9]{1,2})$";
            Pattern p = Pattern.compile(regEx);
            return p.matcher(money).matches();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 判断是否为银行卡号
     */
    public static boolean isIdCardNum(String num) {
        Pattern p = Pattern.compile("^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$|^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(" + "([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X)$");
        Matcher m = p.matcher(num);
        return m.matches();
    }

    /**
     * 判断密码是否合规
     */
    public static boolean isPwd(String pwd) {
        String regEx = "^(?![\\d]+$)(?![a-zA-Z]+$)(?![^\\da-zA-Z]+$).{6,20}$";
        Pattern p = Pattern.compile(regEx);
        return p.matcher(pwd).matches() && !pwd.contains(" ");
    }

    /**
     * 判断密码是否合规
     */
    public static boolean isPayPwd(String pwd) {
        if (TextUtils.isEmpty(pwd)) {
            return false;
        }

        if (pwd.length() == 6) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取资源文件的String
     */
    public static String getRes(int id) {
        return App.getInstance().getString(id);
    }

    /**
     * 隐藏姓名首字母
     */
    public static String hidLastName(String name) {
        if (!TextUtils.isEmpty(name) && name.length() > 1) {
            return "*" + name.substring(1);
        }
        return "";
    }

    private static String getString(int t) {
        String m = "";
        if (t > 0) {
            if (t < 10) {
                m = "0" + t;
            } else {
                m = t + "";
            }
        } else {
            m = "00";
        }
        return m;
    }

    /**
     * 毫秒转换为分秒
     *
     * @param t
     * @return
     */
    public static String format(int t) {
        if (t < 60000) {
            return (t % 60000) / 1000 + "秒";
        } else if ((t >= 60000) && (t < 3600000)) {
            return getString((t % 3600000) / 60000) + "分" + getString((t % 60000) / 1000) + "秒";
        }
        return "";
    }

    /**
     * 判断字符串是否为URL
     *
     * @param urls 用户头像key
     * @return true:是URL、false:不是URL
     */
    public static boolean isUrl(String urls) {
        boolean isurl = false;
        //设置正则表达式
        String regex = "(((https|http)?://)?([a-z0-9]+[.])|(www.))" + "\\w+[.|\\/]([a-z0-9]{0,})?[[.]([a-z0-9]{0,})]+((/[\\S&&[^,;\u4E00-\u9FA5]]+)+)?([.][a-z0-9]{0,}+|/?)";
        Pattern pat = Pattern.compile(regex.trim());//比对
        Matcher mat = pat.matcher(urls.trim());
        isurl = mat.matches();//判断是否匹配
        if (isurl) {
            isurl = true;
        }
        return isurl;
    }

    /**
     * 是否可以设置h5的title（防止h5title设置成网址）
     */
    public static boolean canSetHtmlTitle(String urls) {
        boolean canset = false;
        try {
            if (TextUtils.isEmpty(urls)) {
                canset = false;
            } else if (urls.contains(".") || urls.contains("http") || urls.contains("/")) {
                canset = false;
            } else {
                canset = true;
            }
        } catch (Exception e) {
            canset = false;
        }
        return canset;
    }


    /**
     * 阿拉伯数字金钱转化为大写汉字
     */
    private static final String[] NUMBERS = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    private static final String[] IUNIT = {"元", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟", "万", "拾", "佰", "仟"};
    private static final String[] DUNIT = {"角", "分", "厘"};

    public static String toChinese(String str) {
        try {
            str = str.trim();//// 去掉首尾空格
            str = str.replaceAll(" ", "");// 去掉空格
            str = str.replaceAll(",", "");// 去掉","
            String integerStr;// 整数部分数字
            String decimalStr;// 小数部分数字

            // 初始化：分离整数部分和小数部分
            if (str.indexOf(".") > 0) {
                integerStr = str.substring(0, str.indexOf("."));
                decimalStr = str.substring(str.indexOf(".") + 1);
            } else if (str.indexOf(".") == 0) {
                integerStr = "";
                decimalStr = str.substring(1);
            } else {
                integerStr = str;
                decimalStr = "";
            }
            // integerStr去掉首0，不必去掉decimalStr的尾0(超出部分舍去)
            if (!integerStr.equals("")) {
                integerStr = Long.toString(Long.parseLong(integerStr));
                if (integerStr.equals("0")) {
                    integerStr = "";
                }
            }
            // overflow超出处理能力，直接返回
            if (integerStr.length() > IUNIT.length) {
                System.out.println(str + ":超出处理能力");
                return str;
            }
            int[] integers = toArray(integerStr);// 整数部分数字
            boolean isMust5 = isMust5(integerStr);// 设置万单位
            int[] decimals = toArray(decimalStr);// 小数部分数字
            return getChineseInteger(integers, isMust5) + getChineseDecimal(decimals);
        } catch (Exception e) {
            return str;
        }

    }

    //整数部分和小数部分转换为数组，从高位至低
    private static int[] toArray(String number) {
        int[] array = new int[number.length()];
        for (int i = 0; i < number.length(); i++) {
            array[i] = Integer.parseInt(number.substring(i, i + 1));
        }
        return array;
    }

    //得到中文金额的整数部分。
    private static String getChineseInteger(int[] integers, boolean isMust5) {
        StringBuffer chineseInteger = new StringBuffer("");
        int length = integers.length;
        for (int i = 0; i < length; i++) {
            // 0出现在关键位置：1234(万)5678(亿)9012(万)3456(元)
            // 特殊情况：10(拾元、壹拾元、壹拾万元、拾万元)
            String key = "";
            if (integers[i] == 0) {
                if ((length - i) == 13)// 万(亿)(必填)
                    key = IUNIT[4];
                else if ((length - i) == 9)// 亿(必填)
                    key = IUNIT[8];
                else if ((length - i) == 5 && isMust5)// 万(不必填)
                    key = IUNIT[4];
                else if ((length - i) == 1)// 元(必填)
                    key = IUNIT[0];
                // 0遇非0时补零，不包含最后一位
                if ((length - i) > 1 && integers[i + 1] != 0) key += NUMBERS[0];
            }
            chineseInteger.append(integers[i] == 0 ? key : (NUMBERS[integers[i]] + IUNIT[length - i - 1]));
        }
        return chineseInteger.toString();
    }

    //得到中文金额的小数部分。
    private static String getChineseDecimal(int[] decimals) {
        StringBuffer chineseDecimal = new StringBuffer("");
        for (int i = 0; i < decimals.length; i++) {
            // 舍去3位小数之后的
            if (i == 3) break;
            chineseDecimal.append(decimals[i] == 0 ? "" : (NUMBERS[decimals[i]] + DUNIT[i]));
        }
        return chineseDecimal.toString();
    }

    //判断第5位数字的单位"万"是否应加。
    private static boolean isMust5(String integerStr) {
        int length = integerStr.length();
        if (length > 4) {
            String subInteger = "";
            if (length > 8) {
                // 取得从低位数，第5到第8位的字串
                subInteger = integerStr.substring(length - 8, length - 4);
            } else {
                subInteger = integerStr.substring(0, length - 4);
            }
            return Integer.parseInt(subInteger) > 0;
        } else {
            return false;
        }
    }

}
