package ll.com.gaopincaiiao.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author YuliangZhang
 * @create 2017/6/1.
 * @description BankPlatformMvp
 */

public class NetUtils {
    /**
     *  判断有无网络状态 true有网   false无网络
     * */
    public static boolean isNetConnections(Context context) {
        try {
            if (context == null) return false;
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (manager == null) {
            } else {
                NetworkInfo info = manager.getActiveNetworkInfo();
                if (info != null && info.isAvailable()) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 是否为wifi环境（是WIFI还是手机网络[2G/3G/4G]）
     * @param context
     * @return
     */
    public static boolean isWifiEnvironment(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            int type = networkInfo.getType();
            if (type == ConnectivityManager.TYPE_WIFI) {
                return true;
            } else if (type == ConnectivityManager.TYPE_MOBILE) {
                return false;
            }
        }
        return false;
    }
}
