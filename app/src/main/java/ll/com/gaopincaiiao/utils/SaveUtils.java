package ll.com.gaopincaiiao.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import java.util.Set;

import ll.com.gaopincaiiao.App;


/**
 * @author YuliangZhang
 * @create 2017/6/1.
 * @description 存储工具类
 */

public class SaveUtils {

    private static final String ENCRYPT_KEY="ZWFlNzNhZDUtNjZlYi00MWNiLThkOTUtZjNkY2ZmN2I1N2Q3";//工具类自定义的密钥
    //需要存入的键
    public static final String KEY_PHONE_IMEI = "key_imei";//设备uuid
    public static final String KEY_TOKEN = "key_token";//设备token
    public static final String KEY_IS_SET_GES = "isSetGes";//是否设置手势密码（服务器返回）
    public static final String KEY_IS_LOGIN = "isLogin";//是否登录（本地改动的）
    public static final String KEY_REALNAME_STATUS = "key_real_name_status";//实名认证状态
    public static final String KEY_BANKCARD_STATUS = "KEY_BANKCARD_STATUS";//帮卡状态 0未帮卡 1已帮卡 2其他错误
    public static final String KEY_TIME_STAMP = "timeStamp";//系统当前时间戳，7天超时清手势密码
    public static final String KEY_LOGIN_HINT_GESTURE_TIMES = "loginHintGestureTimes";//登陆完成提示设置手势密码3次后不再弹出
    public static final String KEY_INVITE_CODE = "inviteCode";//h5邀请码
    public static final String KEY_VERSION_GUILD = "key_version_guild";//向导页面弹出判断标记为
    public static final String KEY_POPWINDOW_UPDATE = "key_popwindow_update";//记录首页上次活动内容
    public static final String COVER_ID_NEED_SHOW = "cover_id_need_show192";//记录上次遮层内容
    public static final String LOCATION_LON = "location_lon";//地理经度
    public static final String LOCATION_LAT = "location_lat";//地理纬度
    public static final String LOCATION_CITY = "location_city";//城市名称
    public static final String LOCATION_CITYCODE = "location_citycode";//城市编号
    public static final String KEY_PHONE_NUMBER = "key_phone_number";//手机号
    public static final String KEY_MEMBER_ID = "key_memberId";//会员编号
    //授权状态
    public static final String KEY_YBCCB_STATUS = "key_ybccb_status";//理财
    public static final String KEY_YBCCB_MEMBER_ID = "key_ybccb_memberId";
    public static final String KEY_O2O_STATUS = "key_o2o_status";//生活
    public static final String KEY_O2O_MEMBER_ID = "key_o2o_memberId";
    //小眼睛是否显示资金
    public static final String KEY_CANT_SEE_MONEY ="canseeMoney";
    //底部鸡汤文字
    public static final String KEY_MINE_BOTTOM_TEXT ="KEY_MINE_BOTTOM_TEXT";
    //我的页面生活余额
    public static final String KEY_MINE_LIFE_MONEY ="KEY_MINE_LIFE_MONEY";
    public static final String KEY_USER_REALNAME = "KEY_USER_REALNAME";//用户真实姓名
    public static final String KEY_IDCARD_NUMBER = "KEY_IDCARD_NUMBER";
    public static final String KEY_HIDDEN_IDCARD_NUMBER = "KEY_HIDDEN_IDCARD_NUMBER";
    public static final String NEED_UPDATE_DIALOG = "NEED_UPDATE_DIALOG";
    /**
     * 批量清除用户相关数据
     */
    public static synchronized void clearUserInfo() {
        removePreferences(KEY_TOKEN);
        removePreferences(KEY_INVITE_CODE);
        removePreferences(KEY_MEMBER_ID);
        removePreferences(KEY_IS_LOGIN);
        removePreferences(KEY_IS_SET_GES);
        removePreferences(KEY_YBCCB_STATUS);
        removePreferences(KEY_YBCCB_MEMBER_ID);
        removePreferences(KEY_O2O_STATUS);
        removePreferences(KEY_CANT_SEE_MONEY);
        removePreferences(KEY_MINE_LIFE_MONEY);
        removePreferences(KEY_REALNAME_STATUS);
        removePreferences(KEY_BANKCARD_STATUS);
    }

    /**
     * 获取sp存储
     * */
    public static SharedPreferences sharedPreferences = App.getInstance().getSharedPreferences(App.getInstance().getPackageName(), 0);

    /**
     * 存入数据
     * */
    public static synchronized void putPreferences(String key, Object value) {
        SharedPreferences.Editor editor = SaveUtils.sharedPreferences.edit();
        if(value instanceof String)
        editor.putString(key, (String) value);
        editor.commit();
    }

    public static synchronized void putPreferences(String key, boolean value) {
        SharedPreferences.Editor editor = SaveUtils.sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static synchronized void putPreferences(String key, int value) {
        SharedPreferences.Editor editor = SaveUtils.sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static synchronized void putPreferences(String key, long value) {
        SharedPreferences.Editor editor = SaveUtils.sharedPreferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static synchronized void putPreferences(String key, float value) {
        SharedPreferences.Editor editor = SaveUtils.sharedPreferences.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    //存数组
    public static synchronized void putPreferences(String key, Set<String> value) {
        SharedPreferences.Editor editor = SaveUtils.sharedPreferences.edit();
        editor.putStringSet(key, value);
        editor.commit();
    }


    /**
     * 读出数据
     * */
    public static boolean getPreferences(String key, boolean defaultValue) {
        try {
            return SaveUtils.sharedPreferences.getBoolean(key, defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static int getPreferences(String key, int defValue) {
        try {
            return SaveUtils.sharedPreferences.getInt(key, defValue);
        } catch (Exception e) {
            return defValue;
        }
    }

    public static String getPreferences(String key, String defValue) {
        try {
            return SaveUtils.sharedPreferences.getString(key, defValue);
        } catch (Exception e) {
            return defValue;
        }
    }

    public static long getPreferences(String key, long defValue) {
        try {
            return SaveUtils.sharedPreferences.getLong(key, defValue);
        } catch (Exception e) {
            return defValue;
        }
    }

    public static float getPreferences(String key, float defValue) {
        try {
            return SaveUtils.sharedPreferences.getFloat(key, defValue);
        } catch (Exception e) {
            return defValue;
        }
    }
    //取数组
    public static Set<String> getPreferences(String key, Set<String> defValue) {
        try {
            return SaveUtils.sharedPreferences.getStringSet(key, defValue);
        } catch (Exception e) {
            return defValue;
        }
    }

    /**
     * 用加密方式存入数据
     * */
    public static synchronized void putPreferencesSecret(String key, String value) {

    }
    /**
     * 用加密方式取出数据
     * */
    public static String getPreferencesSecret(String key, String defValue) {
        String value = getPreferences(key, "");
        if (TextUtils.isEmpty(value)) {
            return defValue;
        } else {
            try {
                return "";
            } catch (Exception e) {
                return defValue;
            }
        }
    }

    /**
     * 清除指定数据
     * */
    public static void removePreferences(String key) {
        if (key == null) {
            return;
        }
        SharedPreferences.Editor editor = SaveUtils.sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    /**
     * 清除WebView的cookie
    */
    public static void removeCookie(Context context) {
        CookieSyncManager.createInstance(context);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        CookieManager.getInstance().removeSessionCookie();
        CookieSyncManager.getInstance().sync();
        CookieSyncManager.getInstance().startSync();
    }
}
