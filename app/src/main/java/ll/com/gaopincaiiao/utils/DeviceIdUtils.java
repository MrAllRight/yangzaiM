package ll.com.gaopincaiiao.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author YuliangZhang
 * @create 2017/2/13.
 * @description 手机唯一识别号生成方案, 调用 @DeviceIdUtils.getInstance.getDeviceId() 即可获取
 */

public class DeviceIdUtils {
    private static String LOG_TAG = "DeviceIdUtils";
    private static DeviceIdUtils instance = null;
    private static String deviceId = null;
    private static String IMEI = null;

    /**
     * 单例获取对象
     */
    public static DeviceIdUtils getInstance(Context context) {
        if (instance == null) {
            instance = new DeviceIdUtils(context);
        }
        return instance;
    }

    private DeviceIdUtils(Context context) {
        initDeviceId(context);
    }

    /**
     * 初始化id
     */
    private void initDeviceId(Context context) {
        if (readDeviceIdFile(context)) {//缓存读取成功
            return;
        }
        deviceId = createNewDeviceId(context);
        if (!TextUtils.isEmpty(deviceId)) {
            writeDeviceIdFile(deviceId, context);
        }else {
            Log.e(LOG_TAG,"deviceId生成失败！");
        }
    }

    /**
     * 获取deviceId
     */
    public static String getDeviceId() {
        if (TextUtils.isEmpty(deviceId)){
            Log.e(LOG_TAG,"deviceId获取失败！");
        }
        return deviceId;
    }



    /**
     * 创建新的id
     */
    private String createNewDeviceId(Context context) {
        String packageName = context.getPackageName();
        String androidId = Settings.System.getString(context.getContentResolver(), Settings.System.ANDROID_ID);
        String deviceId = Build.ID;
        String imeiStr = getIMEI(context);
        String additionalStr = null;
        return md5Turns(packageName + androidId + deviceId + imeiStr + additionalStr);
    }


    /**
     * 获取IMEI
     */
    private static String getIMEI(Context context) {
        String imeistring = null;
        try {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                imeistring = tm.getDeviceId();
            }
        } catch (Exception var2) {
            Log.e(LOG_TAG,"IMEI获取异常： "+var2);
        }
        return imeistring;
    }


    /**
     * 将字符串转化为utf-8
     */
    private String md5Turns(String string) {
        byte[] hash1;
        try {
            hash1 = string.getBytes("UTF-8");
        } catch (UnsupportedEncodingException var3) {
            throw new RuntimeException("Huh,UTF-8 should be supported?", var3);
        }
        return computeMD5(hash1);
    }


    /**
     * md5计算
     */
    private String computeMD5(byte[] input) {
        try {
            if (null == input) {
                return null;
            } else {
                MessageDigest e = MessageDigest.getInstance("MD5");
                e.update(input, 0, input.length);
                byte[] md5bytes = e.digest();
                StringBuffer hexString = new StringBuffer();

                for (int i = 0; i < md5bytes.length; ++i) {
                    String hex = Integer.toHexString(255 & md5bytes[i]);
                    if (hex.length() == 1) {
                        hexString.append('0');
                    }
                    hexString.append(hex);
                }
                return hexString.toString();
            }
        } catch (NoSuchAlgorithmException var6) {
            throw new RuntimeException(var6);
        }
    }


    /**
     * deviceid写入文件
     */
    private void writeDeviceIdFile(String devicesId, Context context) {
        if (devicesId != null) {
            FileOutputStream out = null;
            try {
                File installationFile = new File(context.getFilesDir(), "installation");
                out = new FileOutputStream(installationFile, false);
                out.write(devicesId.getBytes("utf-8"));
                out.close();
            } catch (Exception var9) {
                Log.e(LOG_TAG,"deviceId写入文件异常： "+var9);
            }
        }
    }


    /**
     * deviceid读取文件
     */
    private Boolean readDeviceIdFile(Context context) {
        Boolean hasDeviceId = false;
        File installationFile = new File(context.getFilesDir(), "installation");
        if (installationFile.exists()) {
            try {
                FileInputStream fin = new FileInputStream(installationFile);
                BufferedReader buffer = new BufferedReader(new InputStreamReader(fin));
                String text = buffer.readLine();
                if (!TextUtils.isEmpty(text)) {
                    deviceId = text;
                    hasDeviceId = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return hasDeviceId;
    }
}
