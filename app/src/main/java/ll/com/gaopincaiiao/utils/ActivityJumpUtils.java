package ll.com.gaopincaiiao.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import ll.com.gaopincaiiao.R;


/**
 * @author YuliangZhang
 * @create 2017/6/1.
 * @description BankPlatformMvp
 */

public class ActivityJumpUtils {


    /**
     * 通用跳转：默认动画，不传参数,不返回resoult
     * @param curActivity
     * @param nextActivity
     * @param isFinish 是否结束掉当前页面
     */
    public static void commonNext(Activity curActivity, Class<?> nextActivity,boolean isFinish) {
        dealNextCenter(curActivity, nextActivity, null, -1, -1, R.anim.in_from_right, R.anim.out_to_left, isFinish);
    }

    /**
     * 跳转到下一个页面,附带参数
     * @param curActivity
     * @param nextActivity
     * @param extras
     * @param isFinish 是否结束掉当前页面
     */
    public static void nextWithData(Activity curActivity, Class<?> nextActivity,boolean isFinish,Bundle extras) {
        dealNextCenter(curActivity, nextActivity, extras, -1, -1, R.anim.in_from_right, R.anim.out_to_left, isFinish);
    }

    /**
     * 跳转下一个等待结果回调
     * @param curActivity
     * @param nextActivity
     * @param reqCode 请求码
     */
    public static void nextForBack(Activity curActivity, Class<?> nextActivity,int reqCode) {
        dealNextCenter(curActivity, nextActivity, null, reqCode, -1, R.anim.in_from_right, R.anim.out_to_left, false);
    }

    /**
     * 跳转下一个等待结果回调，加参数
     * @param curActivity
     * @param nextActivity
     * @param extras 附带参数
     * @param reqCode      请求码
     */
    public static void nextForBackWithData(Activity curActivity, Class<?> nextActivity, int reqCode,Bundle extras) {
        dealNextCenter(curActivity, nextActivity, extras, reqCode, -1, R.anim.in_from_right, R.anim.out_to_left, false);
    }

    /**
     * 跳转到下一个页面总方法
     *
     * @param curActivity 当前页面
     * @param nextActivity 需要跳转的页面
     * @param extras 附带的参数
     * @param reqCode 请求码
     * @param flag 启动标记
     * @param inAnimId 入场动画
     * @param outAnimId 退场动画
     * @param isFinish 跳转完当前activity是否finish掉
     */
    private static void dealNextCenter(Activity curActivity, Class<?> nextActivity, Bundle extras, int reqCode, int flag, int inAnimId, int
            outAnimId, boolean isFinish) {
        Intent intent = new Intent(curActivity, nextActivity);
        if (null != extras) {
            intent.putExtras(extras);
        }
        if (flag != -1) {
            intent.setFlags(flag);
        }
        if (reqCode < 0) {
            curActivity.startActivity(intent);
        } else {
            curActivity.startActivityForResult(intent, reqCode);
        }
        if (inAnimId != -1 && outAnimId != -1) {
            curActivity.overridePendingTransition(inAnimId, outAnimId);
        }
        if (isFinish) {
            curActivity.finish();
        }
    }

    /**-------------------------------------------------以下为结束页面------------------------------------------------------------*/
    /**
     * 通用结束页面
     * @param curActivity
     */
    public static void commonBack(Activity curActivity) {
        dealBackCenter(curActivity, -1, null, R.anim.in_from_left, R.anim.out_to_right);
    }

    /**
     * 结束页面并且附加结果码（在上一个页面的onActivityResult方法中出现）
     * @param curActivity
     * @param retCode 结果码
     */
    public static void backWithResult(Activity curActivity, int retCode) {
        dealBackCenter(curActivity, retCode, null,  R.anim.in_from_left, R.anim.out_to_right);
    }

    /**
     * 结束页面并且附加结果码（在上一个页面的onActivityResult方法中出现）
     * @param curActivity
     * @param retCode 结果码
     * @param retData 结果数据
     */
    public static void backWithResultAndData(Activity curActivity, int retCode, Bundle retData) {
        dealBackCenter(curActivity, retCode, retData, R.anim.in_from_left, R.anim.out_to_right);
    }

    /**
     * 返回到上一个页面并返回值总方法
     * @param curActivity
     * @param retCode
     * @param retData
     * @param inAnimId
     * @param outAnimId
     */
    private static void dealBackCenter(Activity curActivity, int retCode, Bundle retData, int inAnimId, int outAnimId) {
        Intent intent = null;

        if (retCode != -1) {
            if (null != retData) {//需要返回并且有数据
                curActivity.setResult(retCode, intent);
            }else {//需要返回无数据
                curActivity.setResult(retCode);
            }
        }

        curActivity.finish();
        if (inAnimId != -1 && outAnimId != -1) {
            curActivity.overridePendingTransition(inAnimId, outAnimId);
        }
    }

}
