package ll.com.gaopincaiiao.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import ll.com.gaopincaiiao.App;
import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.activity.login.LoginActivity;


/**
 * @author YuliangZhang
 * @create 2017/5/23.
 * @description BankPlatformMvp
 */

public class CommonDialogUtils {
    private static Toast toast;//吐司
    public static Dialog mProgresDialog;//进度条

    public static Dialog tipDialog;//提示对话框
    public static Dialog tipDoubleClickDialog;//提示对话框(两个按钮)
    public static Dialog tokenErrorDialog;//token失效（跳转登录）
    public static Dialog authorCheckPayPwdDialog;//授权校验支付密码
    public static Dialog logOutDialog;//退出登录
    public static Dialog updateDialog;//更新弹窗
    public static Dialog activityDialog;//首页活动
    public static Dialog  reLoginDialog;
    /**
     * 吐司提醒-------------------------------------------------
     */
    public static void showToast(Context context, String msg) {
        try {
            if (toast != null) {
                toast.cancel();
                toast = null;
            }
            toast = Toast.makeText(context.getApplicationContext(), msg, Toast.LENGTH_SHORT);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 网络加载对话框-----------------------------------------------------
     */
    public static void showProgressDialog(Activity context) {

    }
    /**
     * 网络加载对话框消失
     */
    public static void dismissProgressDialog() {

    }


    /**
     * 网络加载定制对话框-----------------------------------------------------
     */
    public static void showProgressCustomDialog(Activity context, String mess) {

    }

    /**
     * 网络加载定制对话框消失
     */
    public static void dismissProgressCustomDialog() {

    }

    /**
     * 点击跳转登录页面 包括token失效，手势密码失效等------------------------------------------
     * @param context activity
     * @param mess 显示的信息
     * @param finishActivity 点击跳转后是否结束掉当前页
     */
    public static void showClickToLoginDialog(final Context context,String mess,final boolean finishActivity) {

        dismissClickToLoginDialog();//不可设置现在显示就returned防止手势密码划开不显示弹窗
        if (tokenErrorDialog == null) {
            tokenErrorDialog = new Dialog(context, R.style.dm_alert_dialog);
            tokenErrorDialog.setContentView(R.layout.tip_dialog);
        }

        //弹窗配置
        TextView tv = (TextView) tokenErrorDialog.findViewById(R.id.mess);
        tv.setText(mess);
        ((TextView) tokenErrorDialog.findViewById(R.id.dialog_title)).getPaint().setFakeBoldText(true);
//        ((Button) tokenErrorDialog.findViewById(R.id.dia_bt_login)).getPaint().setFakeBoldText(true);
        tokenErrorDialog.findViewById(R.id.dia_bt_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissClickToLoginDialog();
                SaveUtils.clearUserInfo();
                Activity activity = (Activity) context;
                ActivityJumpUtils.commonNext(activity, LoginActivity.class,false);
                if (finishActivity){
                    activity.finish();
                }
            }
        });
        tokenErrorDialog.setCancelable(false);
        tokenErrorDialog.setCanceledOnTouchOutside(false);
        tokenErrorDialog.show();
        setExitKeyListener(tokenErrorDialog,(Activity) context);
    }
    /**
     * 点击跳转登录页面对话框消失
     */
    public static void dismissClickToLoginDialog() {
        if (tokenErrorDialog != null) {
            try {
                tokenErrorDialog.dismiss();
                tokenErrorDialog = null;
            } catch (Exception e) {
            } finally {
                tokenErrorDialog = null;
            }
        }
    }


    /**
     * 普通提示对话框 如密码格式不对---------------------------------------
     */
    public static void showTipDialog(final Context context, String mess) {
        dismissTipDialog();
        if (tipDialog == null) {
            tipDialog = new Dialog(context, R.style.dm_alert_dialog);
            tipDialog.setContentView(R.layout.tip_dialog);
        }
        TextView tv = (TextView) tipDialog.findViewById(R.id.mess);
        tv.setText(mess);
        ((TextView) tipDialog.findViewById(R.id.dialog_title)).getPaint().setFakeBoldText(true);
        tipDialog.findViewById(R.id.dia_bt_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissTipDialog();
            }
        });
        tipDialog.setCancelable(true);
        tipDialog.setCanceledOnTouchOutside(true);
        tipDialog.show();
    }

    /**
     * 普通提示对话框消失 如密码格式不对
     */
    public static void dismissTipDialog() {
        if (tipDialog != null) {
            try {
                tipDialog.dismiss();
                tipDialog = null;
            } catch (Exception e) {
            } finally {
                tipDialog = null;
            }
        }
    }

    /**
     * 普通提示对话框 (下面有两个选择按钮)---------------------------------------
     * @param context
     * @param mess 提示信息内容
     * @param leftButton 左边按钮文字
     * @param rightButton 右边按钮文字
     * @param onclick 点击事件
     */
    public static void showTipDoubleClickDialog(final Context context,String mess,String leftButton,String rightButton, View.OnClickListener onclick) {
        dismissTipDoubleClickDialog();
        if (tipDoubleClickDialog == null) {
            tipDoubleClickDialog = new Dialog(context, R.style.dm_alert_dialog);
            tipDoubleClickDialog.setContentView(R.layout.tip_dialog2);
        }
        //消息内容
        TextView tv = (TextView) tipDoubleClickDialog.findViewById(R.id.mess);
        tv.setText(mess);
        //消息标题
        ((TextView) tipDoubleClickDialog.findViewById(R.id.dialog_title)).getPaint().setFakeBoldText(true);
        //左边（取消）按钮点击事件
        (tipDoubleClickDialog.findViewById(R.id.bt_left)).setOnClickListener(onclick);
        ((Button)tipDoubleClickDialog.findViewById(R.id.bt_left)).setText(leftButton);
        //右边（其他）按钮点击事件
        (tipDoubleClickDialog.findViewById(R.id.bt_right)).setOnClickListener(onclick);
        ((Button)tipDoubleClickDialog.findViewById(R.id.bt_right)).setText(rightButton);
        //弹窗外部点击消失
        tipDoubleClickDialog.setCancelable(true);
        tipDoubleClickDialog.setCanceledOnTouchOutside(true);
        tipDoubleClickDialog.show();
    }

    /**
     * 普通提示对话框消失 如密码格式不对
     */
    public static void dismissTipDoubleClickDialog() {
        if (tipDoubleClickDialog != null) {
            try {
                tipDoubleClickDialog.dismiss();
                tipDoubleClickDialog = null;
            } catch (Exception e) {
            } finally {
                tipDoubleClickDialog = null;
            }
        }
    }


    /**
     * 退出登录对话框-----------------------------------------------
     */
    public static void showLogoutDialog(Activity context, String msg, View.OnClickListener onclick) {
        dismissLogoutDialog();
        if (logOutDialog == null) {
            logOutDialog = new Dialog(context, R.style.dm_alert_dialog);
            logOutDialog.setContentView(R.layout.tip_dialog2);
        }
        TextView tv = (TextView) logOutDialog.findViewById(R.id.mess);
        tv.setText(msg);
        //头部文字加粗
        ((TextView) logOutDialog.findViewById(R.id.dialog_title)).getPaint().setFakeBoldText(true);
        Button bt= (Button) logOutDialog.findViewById(R.id.bt_right);
        bt.setText(context.getResources().getString(R.string.determine));
        bt.setOnClickListener(onclick);
        logOutDialog.findViewById(R.id.bt_left).setOnClickListener(onclick);
        logOutDialog.setCancelable(true);
        logOutDialog.setCanceledOnTouchOutside(true);
        logOutDialog.show();

    }

    public static void dismissLogoutDialog() {
        if (logOutDialog != null) {
            try {
                logOutDialog.dismiss();
                logOutDialog = null;
            } catch (Exception e) {
            } finally {
                logOutDialog = null;
            }
        }


    }


    /**
     * 首页活动弹窗--------------------------------------------------------
     */
    public static void showActivityDialog(Activity context,Dialog dialog) {
        dismissActivityDialog();
        if (activityDialog == null) {
            activityDialog =dialog;
        }
        if (activityDialog != null && !activityDialog.isShowing() && !context.isFinishing()) {
            activityDialog.setCancelable(false);
            activityDialog.show();
        }
    }
    /**
     * 首页活动弹窗消失
     */
    public static void dismissActivityDialog() {
        if (activityDialog != null) {
            try {
                activityDialog.dismiss();
                activityDialog = null;
            } catch (Exception e) {
            } finally {
                activityDialog = null;
            }
        }

    }

  /**
     * 首页更新弹窗--------------------------------------------------------
     */
    public static void showUpdateDialog(Activity context,Dialog dialog) {
        dismissUpdateDialog();
        if (updateDialog == null) {
            updateDialog =dialog;
        }
        if (updateDialog != null && !updateDialog.isShowing() && !context.isFinishing()) {
            updateDialog.setCancelable(false);
            updateDialog.show();
        }
    }
    /**
     * 首页更新弹窗消失
     */
    public static void dismissUpdateDialog() {
        if (updateDialog != null) {
            try {
                updateDialog.dismiss();
                updateDialog = null;
            } catch (Exception e) {
            } finally {
                updateDialog = null;
            }
        }

    }
    /**
     * 授权校验支付密码对话框-（特殊，需要统一管理结束）------------------------------------
     */
    public static void showAuthorPayPwdDialog(Activity context,Dialog dialog) {
        dismissAuthorPayPwdDialog();
        if (authorCheckPayPwdDialog == null) {
            authorCheckPayPwdDialog =dialog;
        }
        if (authorCheckPayPwdDialog != null && !authorCheckPayPwdDialog.isShowing() && !context.isFinishing()) {
            authorCheckPayPwdDialog.setCancelable(false);
            authorCheckPayPwdDialog.show();
        }
    }
    /**
     * 授权校验支付密码对话框消失
     */
    public static void dismissAuthorPayPwdDialog() {
        if (authorCheckPayPwdDialog != null) {
            try {
                authorCheckPayPwdDialog.dismiss();
                authorCheckPayPwdDialog = null;
            } catch (Exception e) {
            } finally {
                authorCheckPayPwdDialog = null;
            }
        }

    }


    /**
     * 清除所有对话框，防止内存泄漏崩溃===============================================
     */
    public static void dismissAllDialogs() {
        dismissTipDialog();
        dismissClickToLoginDialog();
        dismissProgressDialog();
        dismissUpdateDialog();
        dismissActivityDialog();
        dismissLogoutDialog();
        dismissAuthorPayPwdDialog();
    }


    /**
     * 对话框双击返回关闭应用拦截键===============================================
     * */
    private static void setExitKeyListener(Dialog dialog, final Activity activity) {
        if (dialog==null){return;}
        if (activity==null){return;}

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    App.getInstance().doubleClickExit(activity);
                    return true;
                }
                return false;
            }
        });
    }
    public interface DialogCallBack {
        void noWifiUpdateConfirm();
        void noWifiUpdateCancle();
    }
    /**
     * 非wifi提示更新
     */
    public static void showNoWifiUpdate(final Activity activity, String mess, final DialogCallBack callback) {
        if (reLoginDialog == null) {
            reLoginDialog = new Dialog(activity, R.style.dm_alert_dialog);
            reLoginDialog.setContentView(R.layout.nowifi_dialog);
        }
        TextView tv = (TextView) reLoginDialog.findViewById(R.id.mess);
        tv.setText(mess);
        ((TextView) reLoginDialog.findViewById(R.id.dialog_title)).getPaint().setFakeBoldText(true);
//        ((Button) reLoginDialog.findViewById(R.id.dia_bt_login)).getPaint().setFakeBoldText(true);
        reLoginDialog.findViewById(R.id.dia_bt_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) callback.noWifiUpdateConfirm();
                reLoginDialog.dismiss();
            }
        });
        reLoginDialog.findViewById(R.id.dia_bt_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) callback.noWifiUpdateCancle();
                reLoginDialog.dismiss();
            }
        });
        reLoginDialog.setCancelable(false);
        reLoginDialog.setCanceledOnTouchOutside(false);
        reLoginDialog.show();
    }
}
