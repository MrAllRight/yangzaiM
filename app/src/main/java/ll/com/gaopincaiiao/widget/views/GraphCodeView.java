package ll.com.gaopincaiiao.widget.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.utils.UiUtils;

/**
 * @author YuliangZhang
 * @create 2017/6/6.
 * @description 图形验证码视图
 */

public class GraphCodeView extends View {

    private Random mRandom = new Random();
    private Paint mTextPaint; // 文本画笔
    private ArrayList<Path> mPaths = new ArrayList<Path>();//干扰线集合容器
    private Paint mPathPaint;//干扰线画笔

    private String mCodeString = "0000";//验证码字符串默认4位
    private int mWidth;//控件的宽度
    private int mHeight; //控件的高度
    private float mTextWidth;//验证码字符串的显示宽度

    private int pathLineCount = 3;//干扰线条数
    private float mTextSize= UiUtils.dp2px(22);//验证码字符的大小dp
    private float pathPaintWidth=2;//干扰线粗细
    private float textPaintWidth=1;//文字线条粗细

    /**
     * 在java代码中创建view的时候调用，即new
     */
    public GraphCodeView(Context context) {
        this(context, null);
        init(context);
    }

    /**
     * 在xml布局文件中使用view但没有指定style的时候调用
     */
    public GraphCodeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        getAttrValues(context, attrs);
        init(context);
    }

    /**
     * 在xml布局文件中使用view并指定style的时候调用
     */
    public GraphCodeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getAttrValues(context, attrs);
        init(context);
    }

    /**
     * 获取布局文件中的值
     *
     * @param context
     */
    private void getAttrValues(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.GraphCodeView);
        mTextSize=typedArray.getDimension(R.styleable.GraphCodeView_codeTextSize,mTextSize);
        textPaintWidth=typedArray.getDimension(R.styleable.GraphCodeView_textPainWidth,textPaintWidth);
        pathPaintWidth=typedArray.getDimension(R.styleable.GraphCodeView_pathPainWidth,pathPaintWidth);
        pathLineCount=typedArray.getInteger(R.styleable.GraphCodeView_pathLines,pathLineCount);
        typedArray.recycle();//资源回收
    }

    private boolean needInvalidate = true;//是否需要重新绘制

    @Override
    protected void onDraw(Canvas canvas) {
        // 获取控件的宽和高，此时已经测量完成
        mHeight = getHeight();
        mWidth = getWidth();
        drawTextCode(canvas);
        drawPathLine(canvas);
    }

    /**
     * 绘制文字
     */
    private void drawTextCode(Canvas canvas) {
        float charLength = (mWidth - mTextWidth) / (mCodeString.length() + 1);//文字间距
        for (int i = 1; i <= mCodeString.length(); i++) {
            int offsetDegree = mRandom.nextInt(15);
            //画布以控件中心为原点随机旋转
            offsetDegree = mRandom.nextInt(2) == 1 ? offsetDegree : -offsetDegree;
            canvas.save();
            canvas.rotate(offsetDegree, mWidth / 2, mHeight / 2);
            // 给画笔设置随机颜色
            mTextPaint.setARGB(255, mRandom.nextInt(200) + 20, mRandom.nextInt(200) + 20, mRandom.nextInt(200) + 20);
            canvas.drawText(String.valueOf(mCodeString.charAt(i - 1)), i * charLength + mTextWidth / (mCodeString.length()) * (i - 1), mHeight * 2 / 3f, mTextPaint);
            canvas.restore();
        }
    }

    /**
     * 绘制干扰线
     */
    private void drawPathLine(Canvas canvas) {
        // 生成干扰线坐标
        for (int i = 0; i < pathLineCount; i++) {
            Path path = new Path();
            int startX = mRandom.nextInt(mWidth / 3) + 10;
            int startY = mRandom.nextInt(mHeight / 3) + 10;
            int endX = mRandom.nextInt(mWidth / 2) + mWidth / 2 - 10;
            int endY = mRandom.nextInt(mHeight / 2) + mHeight / 2 - 10;
            path.moveTo(startX, startY);
            path.quadTo(Math.abs(endX - startX) / 2, Math.abs(endY - startY) / 2, endX, endY);
            mPathPaint.setARGB(255, mRandom.nextInt(200) + 20, mRandom.nextInt(200) + 20, mRandom.nextInt(200) + 20);
            canvas.drawPath(path, mPathPaint);
        }
    }

    /**
     * 初始化一些数据
     */
    private void init(Context context) {
        // 初始化文字画笔
        mTextPaint = new Paint();
        mTextPaint.setStrokeWidth(textPaintWidth); // 画笔大小为3
        mTextPaint.setTextSize(mTextSize); // 设置文字大小
        // 初始化干扰线画笔
        mPathPaint = new Paint();
        mPathPaint.setStrokeWidth(pathPaintWidth);
        mPathPaint.setColor(Color.GRAY);
        mPathPaint.setStyle(Paint.Style.STROKE); // 设置画笔为空心
        mPathPaint.setStrokeCap(Paint.Cap.ROUND); // 设置断点处为圆形
        // 取得验证码字符串显示的宽度值
        mTextWidth = mTextPaint.measureText(mCodeString);
    }

    /**
     * 要像layout_width和layout_height属性支持wrap_content就必须重新这个方法
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // 分别测量控件的宽度和高度，基本为模板方法
        int measureWidth = measureWidth(widthMeasureSpec);
        int measureHeight = measureHeight(heightMeasureSpec);
        // 将测量出来的宽高设置进去完成测量
        setMeasuredDimension(measureWidth, measureHeight);
    }

    /**
     * 测量宽度
     */
    private int measureWidth(int widthMeasureSpec) {
        int result = (int) (mTextWidth * 1.8f);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        if (widthMode == MeasureSpec.EXACTLY) {
            // 精确测量模式，即布局文件中layout_width或layout_height一般为精确的值或match_parent
            result = widthSize; // 既然是精确模式，那么直接返回测量的宽度即可
        } else {
            if (widthMode == MeasureSpec.AT_MOST) {
                // 最大值模式，即布局文件中layout_width或layout_height一般为wrap_content
                result = Math.min(result, widthSize);
            }
        }
        return result;
    }

    /**
     * 测量高度
     */
    private int measureHeight(int heightMeasureSpec) {
        int result = (int) (mTextWidth / 1.6f);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        if (heightMode == MeasureSpec.EXACTLY) {
            // 精确测量模式，即布局文件中layout_width或layout_height一般为精确的值或match_parent
            result = heightSize; // 既然是精确模式，那么直接返回测量的宽度即可
        } else {
            if (heightMode == MeasureSpec.AT_MOST) {
                // 最大值模式，即布局文件中layout_width或layout_height一般为wrap_content
                result = Math.min(result, heightSize);
            }
        }
        return result;
    }

    /**
     * 外部设置文字
     */
    public void setText(String text) {
        mCodeString = text;
        mTextWidth = mTextPaint.measureText(mCodeString);
        needInvalidate = true;
        invalidate();
    }
}
