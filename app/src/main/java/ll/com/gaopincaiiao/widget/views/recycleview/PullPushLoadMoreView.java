package ll.com.gaopincaiiao.widget.views.recycleview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lcodecore.tkrefreshlayout.IBottomView;

import ll.com.gaopincaiiao.R;


/**
 * Created by liuyong
 * Data: 2018/5/14
 * Github:https://github.com/MrAllRight
 */

public class PullPushLoadMoreView extends FrameLayout implements IBottomView {
    private Context context;
    private TextView tv;
    private boolean isNoMore = false;

    public PullPushLoadMoreView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public PullPushLoadMoreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(context).inflate(R.layout.item_foot_loadmore, this);
        tv = (TextView) view.findViewById(R.id.tv_no_more);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public void onPullingUp(float fraction, float maxBottomHeight, float bottomHeight) {

    }

    @Override
    public void startAnim(float maxBottomHeight, float bottomHeight) {
        if (!isNoMore)
            tv.setText(context.getString(R.string.loading_more));
        else tv.setText(context.getString(R.string.no_more));
    }

    @Override
    public void onPullReleasing(float fraction, float maxBottomHeight, float bottomHeight) {

    }

    @Override
    public void onFinish() {
        if (!isNoMore)
            tv.setText(context.getString(R.string.load_more));
        else tv.setText(context.getString(R.string.no_more));
    }

    @Override
    public void reset() {
        if (!isNoMore)
            tv.setText(context.getString(R.string.load_more));
        else tv.setText(context.getString(R.string.no_more));
    }

    public void setNoMore(boolean noMore) {
        isNoMore = noMore;
        requestLayout();
    }
}
