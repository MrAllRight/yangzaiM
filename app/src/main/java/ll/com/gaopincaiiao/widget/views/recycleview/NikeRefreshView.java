package ll.com.gaopincaiiao.widget.views.recycleview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.utils.UiUtils;


/**
 * @author YuliangZhang
 * @create 2017/8/26.
 * @description BaseFrame
 */

public class NikeRefreshView extends View {
    // 画笔
    private Paint mPaint;
    //值动画
    private ValueAnimator mRefreshingAnimator, mComplteAnimator;
    private ValueAnimator.AnimatorUpdateListener mUpdateListener;
    // View 宽高
    private int mViewWidth, mViewHeight;
    //路径
    private Path pathCircle, pathGou;
    //路径测量器
    private PathMeasure mMeasure, mMeasureGou;
    // 当前动画数值
    private float mAnimatorValue = 0;
    //当前状态
    private State mCurrentState = State.NORMAL;


    // 这个视图拥有的状态(下拉，刷新中，刷新完成，平常状态)
    public static enum State {
        PULL, REFRESHING, COMPLETE, NORMAL
    }

    public NikeRefreshView(Context context) {
        this(context, null);
    }

    public NikeRefreshView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initAll();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mViewHeight = MeasureSpec.getSize(heightMeasureSpec);
        mViewWidth = MeasureSpec.getSize(widthMeasureSpec);
    }

    /**
     * 初始化
     */
    private void initAll() {
        //初始化画笔
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(getResources().getColor(R.color.color_blue_btn_release));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(5);
        // 外部圆环
        pathCircle = new Path();
        RectF oval2 = new RectF(-UiUtils.dp2px(10), -UiUtils.dp2px(10), UiUtils.dp2px(10), UiUtils.dp2px(10));
        pathCircle.addArc(oval2, -90, 359.9f);
        // 对勾
        pathGou = new Path();
        pathGou.moveTo(-UiUtils.dp2px(5f), UiUtils.dp2px(5f));
        pathGou.lineTo(0, UiUtils.dp2px(10));
        pathGou.lineTo(UiUtils.dp2px(10), UiUtils.dp2px(0));
        //路径截取器
        mMeasure = new PathMeasure();
        mMeasure.setPath(pathCircle, false);
        mMeasureGou = new PathMeasure();
        mMeasureGou.setPath(pathGou, false);
        //动画刷新回调
        mUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mAnimatorValue = (float) animation.getAnimatedValue();
                invalidate();
            }
        };
    }

    /**
     * 恢复一般状态
     */
    public void setNormalState() {
        mCurrentState = State.NORMAL;
        if (mRefreshingAnimator != null) mRefreshingAnimator.end();
        if (mComplteAnimator != null) mComplteAnimator.end();
    }

    /**
     * 设置下拉状态和下拉值
     */
    public void setPullValue(float value) {
        mCurrentState = State.PULL;
        mAnimatorValue = value;
        invalidate();
    }

    /**
     * 设置正在刷新状态
     */
    public void setRefreshingState(int duration) {
        mCurrentState = State.REFRESHING;
        mRefreshingAnimator = ValueAnimator.ofFloat(0, 1).setDuration(duration);
        mRefreshingAnimator.addUpdateListener(mUpdateListener);
        mRefreshingAnimator.start();
        mRefreshingAnimator.setRepeatCount(500);
    }

    /**
     * 设置刷新完成nike
     */
    public void setCompleteState(int duration) {
        mCurrentState = State.COMPLETE;
        mComplteAnimator = ValueAnimator.ofFloat(0, 1).setDuration(duration);
        mComplteAnimator.addUpdateListener(mUpdateListener);
        mComplteAnimator.start();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate(mViewWidth / 2, mViewHeight / 2);
        switch (mCurrentState) {
            case PULL:
                mMeasure.setPath(pathCircle, false);
                Path dst = new Path();
                float stop1 = mMeasure.getLength() * mAnimatorValue;
                float start1 = 0;
                mMeasure.getSegment(start1, stop1, dst, true);
                canvas.drawPath(dst, mPaint);
                break;
            case REFRESHING:
                mMeasure.setPath(pathCircle, false);
                Path dst2 = new Path();
                float stop = mMeasure.getLength() * mAnimatorValue;
                float start = (float) (stop - ((0.5 - Math.abs(mAnimatorValue - 0.5)) * 600f));
                mMeasure.getSegment(start, stop, dst2, true);
                canvas.drawPath(dst2, mPaint);
                break;
            case COMPLETE:
                Path dst3 = new Path();
                float stop2 = mMeasure.getLength() * mAnimatorValue;
                float start2 = 0;
                mMeasureGou.getSegment(start2, stop2, dst3, true);
                canvas.drawPath(dst3, mPaint);
                break;
        }
    }
}
