package ll.com.gaopincaiiao.widget.views.recycleview;


import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuyong on 2016/11/24.
 */

public abstract class BaseRecyclerViewAdaper<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_NORMAL = 1;
    public static final int TYPE_FOOTER = 2;
    private boolean isCanTouch = false;

    public abstract RecyclerView.ViewHolder onCreate(ViewGroup parent, final int viewType);

    public abstract void onBind(RecyclerView.ViewHolder viewHolder, int RealPosition, T data);

    public ArrayList<T> mDatas = new ArrayList<>();
    private ArrayList<View> mHeaderViews = new ArrayList<>();
    private ArrayList<View> mFooterViews = new ArrayList<>();
    private int i = -1, j = -1;

    public void setCanTouch(boolean canTouch) {
        isCanTouch = canTouch;
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
    }

    public interface OnItemLongClickLitener {
        void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;
    private OnItemLongClickLitener mOnItemLongClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    public void setOnItemLongClickLitener(OnItemLongClickLitener mOnItemLongClickLitener) {
        this.mOnItemLongClickLitener = mOnItemLongClickLitener;
    }

    public void addHeaderView(View view) {
        mHeaderViews.add(mHeaderViews.size(), view);
    }

    public void addFooterView(View view) {
        mFooterViews.add(mFooterViews.size(), view);
    }

    public void addDatas(List<T> datas) {
        mDatas.clear();
        mDatas.addAll(datas);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            i++;
            RecyclerView.ViewHolder headHolder = new Holder(mHeaderViews.get(i));
            headHolder.itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return isCanTouch;
                }
            });
            return headHolder;
        } else if (viewType == TYPE_FOOTER) {
            j++;
            RecyclerView.ViewHolder footHolder = new Holder(mFooterViews.get(j));
            footHolder.itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return isCanTouch;
                }
            });
            return footHolder;
        }
        RecyclerView.ViewHolder holder = onCreate(parent, viewType);
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isCanTouch;
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_HEADER) return;
        if (getItemViewType(position) == TYPE_FOOTER) return;
        final int pos = getRealPosition(holder);
        final T data = mDatas.get(pos);
        onBind(holder, pos, data);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickLitener == null) return;
                mOnItemClickLitener.onItemClick(holder.itemView, pos);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mOnItemClickLitener == null) return true;
                mOnItemLongClickLitener.onItemLongClick(holder.itemView, pos);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatas.size() + mHeaderViews.size() + mFooterViews.size();
    }

    public int getRealPosition(RecyclerView.ViewHolder holder) {
        int position = holder.getLayoutPosition();
        return position - mHeaderViews.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mHeaderViews.size()) return TYPE_HEADER;
        if (position >= mHeaderViews.size() + mDatas.size()) return TYPE_FOOTER;
        return TYPE_NORMAL;
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }
    }
}
