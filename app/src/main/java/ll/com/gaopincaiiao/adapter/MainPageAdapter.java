package ll.com.gaopincaiiao.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ll.com.gaopincaiiao.fragment.HomeFragment;
import ll.com.gaopincaiiao.fragment.MineFragment;

/**
 * Created by liuyong
 * Data: 2018/6/6
 * Github:https://github.com/MrAllRight
 */

public class MainPageAdapter extends FragmentPagerAdapter {
    private int count;
    private HomeFragment homeFragment;
    private MineFragment mineFragment;

    public MainPageAdapter(FragmentManager fm, int count) {
        super(fm);
        this.count = count;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (homeFragment == null) homeFragment = new HomeFragment();
            return homeFragment;
        } else if (mineFragment == null) mineFragment = new MineFragment();
        return mineFragment;
    }

    @Override
    public int getCount() {
        return count;
    }
}
