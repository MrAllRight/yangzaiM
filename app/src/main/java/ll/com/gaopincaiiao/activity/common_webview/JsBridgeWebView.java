package ll.com.gaopincaiiao.activity.common_webview;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.DefaultHandler;




/**
 * @author YuliangZhang
 * @create 2018/5/16.
 * @description Zhonglv_bank_v2.0
 */

public class JsBridgeWebView extends BridgeWebView {

    private Context context;

    public JsBridgeWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public JsBridgeWebView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public JsBridgeWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public void init() {
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        this.setDefaultHandler(new DefaultHandler());
        WebSettings settings = this.getSettings();
        settings.setJavaScriptEnabled(true);// 支持JavaScript
        settings.setSaveFormData(false);// 不保存数据
        settings.setSavePassword(false);// 不可保存密码
        settings.setSupportZoom(true);// 不可缩放
        settings.setUseWideViewPort(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLoadsImagesAutomatically(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT); // 设置缓存
        settings.setDomStorageEnabled(true); // 开启 DOM storage API 功能
        settings.setDatabaseEnabled(true);

        if (Build.VERSION.SDK_INT < 19) {
            settings.setLoadsImagesAutomatically(false);
        }

        // 设置数据库缓存路径
        String cacheDirPath = context.getFilesDir().getAbsolutePath() +"ssss";
        settings.setDatabasePath(cacheDirPath);
        // 设置 Application Caches 缓存目录
        settings.setAppCachePath(cacheDirPath);
        // 开启 Application Caches 功能
        settings.setAppCacheEnabled(true);
        // 设置监听事件
        String ua = settings.getUserAgentString();
        settings.setUserAgentString(ua + " webview");
        this.setDownloadListener(new WebViewDownLoadListener());
    }

    /**
     * 下载器
     * */
    private class WebViewDownLoadListener implements DownloadListener {
        @Override
        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            (context).startActivity(intent);
        }
    }
}
