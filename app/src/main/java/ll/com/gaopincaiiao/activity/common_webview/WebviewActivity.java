package ll.com.gaopincaiiao.activity.common_webview;

import android.app.Activity;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import cn.jpush.android.api.JPushInterface;
import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.utils.SaveUtils;

/**
 * Created by liuyong
 * Data: 2018/7/5
 * Github:https://github.com/MrAllRight
 */

public class WebviewActivity extends Activity implements View.OnClickListener {
    WebView webView;
    String homeUrl = "";
    ImageView back, go, home, refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//隐藏状态栏
        setContentView(R.layout.activity_web);

        // 初始化 JPush。如果已经初始化，但没有登录成功，则执行重新登录。
        JPushInterface.init(getApplicationContext());
        back = (ImageView) findViewById(R.id.back);
        go = (ImageView) findViewById(R.id.go);
        refresh = (ImageView) findViewById(R.id.refresh);
        home = (ImageView) findViewById(R.id.home);
        back.setOnClickListener(this);
        go.setOnClickListener(this);
        home.setOnClickListener(this);
        refresh.setOnClickListener(this);
        webView = (WebView) findViewById(R.id.webview);
        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);// 支持JavaScript
        settings.setSaveFormData(false);// 不保存数据
        settings.setSavePassword(false);// 不可保存密码
        settings.setSupportZoom(true);// 不可缩放
        settings.setUseWideViewPort(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLoadsImagesAutomatically(true);
        // 设置缓存
        settings.setCacheMode(WebSettings.LOAD_DEFAULT); // 设置
        // 缓存模式
        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        // 开启 database storage API 功能
        settings.setDatabaseEnabled(true);
        String cacheDirPath = getFilesDir().getAbsolutePath() + "zlbank";
        // 设置数据库缓存路径
        settings.setDatabasePath(cacheDirPath);
        // 设置 Application Caches 缓存目录
        settings.setAppCachePath(cacheDirPath);
        // 开启 Application Caches 功能
        settings.setAppCacheEnabled(true);
        // 设置监听事件
        String ua = settings.getUserAgentString();
        settings.setUserAgentString(ua + " webview");
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (TextUtils.isEmpty(url) || (!TextUtils.isEmpty(Uri.parse(url).getScheme()) && Uri.parse(url)
                        .getScheme().equals("mailto")))
                    return true;
                else if (url.startsWith("yy://")) return super.shouldOverrideUrlLoading(view, url);
                else view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });
        homeUrl = SaveUtils.getPreferences("url", "");
        webView.loadUrl(homeUrl);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (webView.canGoBack()) webView.goBack();
            else finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                if (webView.canGoBack()) webView.goBack();
                break;
            case R.id.go:
                if (webView.canGoForward()) webView.goForward();
                break;
            case R.id.home:
                webView.loadUrl(homeUrl);
                break;
            case R.id.refresh:
                webView.reload();
                break;
        }
    }
}
