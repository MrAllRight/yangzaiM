package ll.com.gaopincaiiao.activity.change_login_pwd;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.utils.CommonDialogUtils;
import ll.com.gaopincaiiao.utils.SaveUtils;
import ll.com.gaopincaiiao.utils.StringUtils;

public class ChangeLoginPwdActivity extends Activity{


    private String oldPwd, newPwd;

    @BindView(R.id.et_old_login_pwd)
    EditText etOldPwd;
    @BindView(R.id.et_new_login_pwd)
    EditText etNewPwd;

    @BindView(R.id.modify_confirm)
    Button btConfirm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_login_pwd);
        ButterKnife.bind(this);
        initConfigs();
    }



    protected void initConfigs() {
        ((TextView)findViewById(R.id.abCenterTex)).setText("修改登入密码");
        showPassWord(false, etNewPwd);
        showPassWord(false, etOldPwd);
    }

    /**
     * 小眼睛点击，秘密是否可见
     */
    @OnCheckedChanged({ R.id.cb_new_pay_pass, R.id.cb_old_pay_pass})
    public void canSeePwd(CompoundButton buttonView, boolean canSee) {
        switch (buttonView.getId()) {
            case R.id.cb_old_pay_pass:
                showPassWord(canSee, etOldPwd);
                break;
            case R.id.cb_new_pay_pass:
                showPassWord(canSee, etNewPwd);
                break;
        }
    }

    @OnClick(R.id.modify_confirm)
    public void changePwd() {
        oldPwd = etOldPwd.getText().toString();
        newPwd = etNewPwd.getText().toString();

        if (TextUtils.isEmpty(oldPwd)) {
            CommonDialogUtils.showToast(this, getString(R.string.old_login_pass_not_null));
            return;
        }
        if (TextUtils.isEmpty(newPwd)) {
            CommonDialogUtils.showToast(this, getString(R.string.new_login_pass_not_null));
            return;
        }


        if (!StringUtils.isPwd(newPwd)) {
            CommonDialogUtils.showToast(this, getString(R.string.pwd_not_right));
            return;
        }

        if (oldPwd.equals(newPwd)) {
            CommonDialogUtils.showToast(this, getString(R.string.pwd_equal_error));
            return;
        }



        String mobilePhone = SaveUtils.getPreferencesSecret(SaveUtils.KEY_PHONE_NUMBER, "");
//        presenter.changeLoginPwd(mobilePhone,newPwd,oldPwd);
    }

    /**
     * 密码可见公用方法
     */
    public void showPassWord(boolean canSee, EditText etView) {
        if (canSee) {
            etView.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            etView.setSelection(etView.getText().length());
        } else {
            etView.setTransformationMethod(PasswordTransformationMethod.getInstance());
            etView.setSelection(etView.getText().length());
        }
    }
}
