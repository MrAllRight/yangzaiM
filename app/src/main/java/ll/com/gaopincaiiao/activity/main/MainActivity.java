package ll.com.gaopincaiiao.activity.main;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.adapter.MainPageAdapter;
import ll.com.gaopincaiiao.fragment.HomeFragment;


/**
 * Created by liuyong
 * Data: 2018/6/6
 * Github:https://github.com/MrAllRight
 */

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;

    TabLayout tabLayout;
    MainPageAdapter adapter;
    private String tabTitles[] = new String[]{};
    private int drawable[] = new int[]{R.mipmap.home_icon, R.mipmap.home_wode_icon};
    private Fragment fg;
   private int index=1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        initConfigs();
    }


    protected void initConfigs() {
        ((TextView) findViewById(R.id.abCenterTex)).setText(getString(R.string.app_name));

        adapter = new MainPageAdapter(getSupportFragmentManager(), 2);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabTitles = new String[]{"福利中心", "我的设置"};
        setTabLayoutView();
        setTablayoutSelectEvent();
    }

    private void setTabLayoutView() {
        tabLayout.getTabAt(0).setCustomView(getTabview(0));
        tabLayout.getTabAt(1).setCustomView(getTabview(1));
    }

    private View getTabview(int positon) {
        View view = LayoutInflater.from(this).inflate(R.layout.tabview, null);
        TextView tv = (TextView) view.findViewById(R.id.tv);
        ImageView iv = (ImageView) view.findViewById(R.id.iv);
        tv.setText(tabTitles[positon]);
        iv.setImageDrawable(getResources().getDrawable(drawable[positon]));
        return view;
    }

    /**
     * tablayout 选中事件处理
     */
    private void setTablayoutSelectEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                changeTabSelect(tab, true);
                index=tab.getPosition()+1;
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                changeTabSelect(tab, false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void changeTabSelect(TabLayout.Tab tab, boolean isSelect) {
        View view = tab.getCustomView();
        ImageView iv = (ImageView) view.findViewById(R.id.iv);
        if (tab.getPosition() == 0) {
            index=1;
            if (isSelect) {
                ((TextView) findViewById(R.id.abCenterTex)).setText(getString(R.string.app_name));
                iv.setImageDrawable(getResources().getDrawable(R.mipmap.home_icon));
            } else iv.setImageDrawable(getResources().getDrawable(R.mipmap.home_icon_d));
        } else if (tab.getPosition() == 1) {
            index=2;
            if (isSelect) {
                ((TextView) findViewById(R.id.abCenterTex)).setText("我的设置");
                iv.setImageDrawable(getResources().getDrawable(R.mipmap.home_wode_icon_d));
            } else iv.setImageDrawable(getResources().getDrawable(R.mipmap.home_wode_icon));
        }
    }


    public void finishSelf() {
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (index==1) {
            ((HomeFragment)adapter.getItem(0)).onKeyDown(keyCode, event);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
