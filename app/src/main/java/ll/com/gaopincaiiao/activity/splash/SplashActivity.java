package ll.com.gaopincaiiao.activity.splash;

/**
 * Created by liuyong
 * Data: 2018/7/4
 * Github:https://github.com/MrAllRight
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;
import cn.jpush.android.api.JPushInterface;
import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.activity.common_webview.WebviewActivity;
import ll.com.gaopincaiiao.activity.login.LoginActivity;
import ll.com.gaopincaiiao.utils.ActivityJumpUtils;
import ll.com.gaopincaiiao.utils.SaveUtils;


public class SplashActivity extends Activity {
    private DisplayImageOptions options;//imageloder配置信息
    private int versionGuide = 0;
    private String imageType = "03";
    private CountDownTimer timer;
    private static final long splashTime = 3 * 1000;//闪屏时间
    private int currentTime = 0;//当前时间（最好和闪屏时间一样）
    @BindView(R.id.start_iv)
    ImageView imStart;//银行logo
    private TextView tvSpalsh;
    private String homeUrl = "file:///android_asset/www/index.html";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_splash);
        initConfigs();
    }


    protected void initConfigs() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//隐藏状态栏
        versionGuide = SaveUtils.getPreferences(SaveUtils.KEY_VERSION_GUILD, 0);
        JPushInterface.init(getApplicationContext());
        initImageLoaderOption();
        load();
    }

    public void load() {
        new Thread(new Runnable() {

            BufferedReader reader = null;
            HttpURLConnection connection = null;

            @Override
            public void run() {
                try {
                    Looper.prepare();
                    URL url = null;
                    url = new URL("http://ios1.goodview.info:8001/ios1_1_cptzz.jsp?ios1_3_gpcptz");
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(5000);
                    connection.setReadTimeout(5000);
                    connection.setRequestMethod("GET");
                    InputStream in = connection.getInputStream();
                    reader = new BufferedReader(new InputStreamReader(in));
                    final StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                homeUrl = new JSONObject(response.toString()).optString("retMsg");
                                if (TextUtils.isEmpty(homeUrl) || homeUrl.contains("goodview.info") || !Patterns.WEB_URL.matcher(homeUrl).matches()) {
                                    SaveUtils.putPreferences("url", "file:///android_asset/www/index.html");
                                    startView();
                                } else {
                                    SaveUtils.putPreferences("url", homeUrl);
                                    ActivityJumpUtils.commonNext(SplashActivity.this, WebviewActivity.class, true);
                                }
                            } catch (final JSONException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        SaveUtils.putPreferences("url", "file:///android_asset/www/index.html");
                                        startView();
                                    }
                                });
                                e.printStackTrace();
                            }
                        }
                    });

                } catch (final MalformedURLException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SaveUtils.putPreferences("url", "file:///android_asset/www/index.html");
                            startView();
                        }
                    });

                    e.printStackTrace();
                } catch (final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SaveUtils.putPreferences("url", "file:///android_asset/www/index.html");
                            startView();
                        }
                    });
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }
        }).start();


    }


    public void startView() {
        Intent intent = new Intent();
        intent.setClass(this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        finish();
    }

    public void initImageLoaderOption() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.start)//加载过程中显示的图片
                .showImageForEmptyUri(R.mipmap.start)//加载内容为空显示的图片
                .showImageOnFail(R.mipmap.start)//加载失败显示的图片
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .delayBeforeLoading(700)//加载前延时
                .build();
    }




    /**
     * 开启后台定时操作
     */
    private void startTimer() {
        if (timer == null) {
            timer = new CountDownTimer(splashTime, 500) {
                @Override
                public void onTick(long leftTime) {
                    currentTime = (int) leftTime / 1000 + 1;
                    tvSpalsh.setText(currentTime + "s" + " 跳过");

                    if (currentTime <= 1) {
                        startView();
                    }

                }

                @Override
                public void onFinish() {//在这里跳转回慢一拍
                }
            };
        }
        timer.start();
    }

    /**
     * 关闭后台定时操作
     */
    private void cancelTimer() {
        if (timer != null) {//关闭定时器
            timer.cancel();
        }
    }

}

