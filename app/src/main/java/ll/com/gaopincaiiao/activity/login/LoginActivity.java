package ll.com.gaopincaiiao.activity.login;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.SignUpCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.activity.main.MainActivity;
import ll.com.gaopincaiiao.utils.ActivityJumpUtils;
import ll.com.gaopincaiiao.utils.CommonDialogUtils;
import ll.com.gaopincaiiao.utils.StringUtils;
import ll.com.gaopincaiiao.widget.views.GraphCodeView;

/**
 * Created by liuyong
 * Data: 2018/5/28
 * Github:https://github.com/MrAllRight
 */

public class LoginActivity extends Activity implements CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.create_account)
    TextView tvCreateAccount;
    @BindView(R.id.login_edt_username)
    EditText etUserName;//用户名
    @BindView(R.id.login_edt_password)
    EditText etUserPass;//密码
    @BindView(R.id.login_edt_check_password)
    EditText etVerficationCode;//图形验证码
    @BindView(R.id.rl_code_check)
    RelativeLayout rlVerficationCode;//图形验证码布局
    @BindView(R.id.bt_login)
    Button btLogin;//登陆按钮
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.login_cbx_password)
    CheckBox cbxSeePassWord;//密码小眼睛

    @BindView(R.id.codeview)
    GraphCodeView codeView;//图形验证码
    @BindView(R.id.login_img_delete)
    ImageView imDelete;//删除
    boolean isRegist = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//隐藏状态栏
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initConfigs();
    }


    protected void initConfigs() {
         isRegist = getIntent().getBooleanExtra("isRegist", false);
        if (isRegist) {
            tvTitle.setText("欢迎注册" + getString(R.string.app_name) + "账户");
            btLogin.setText("注册");
            tvCreateAccount.setText("已有账户去登入");
        }
        cbxSeePassWord.setOnCheckedChangeListener(this);
        showPassWordVisible(false);
    }

    public void showPassWordVisible(boolean canSee) {
        if (canSee) {
            etUserPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            etUserPass.setSelection(etUserPass.getText().length());
        } else {
            etUserPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            etUserPass.setSelection(etUserPass.getText().length());
        }
    }

    /**
     * 进去注册
     */
    @OnClick(R.id.create_account)
    void onCreateAccount() {
        if(isRegist){
            ActivityJumpUtils.commonNext(LoginActivity.this, LoginActivity.class, true);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putBoolean("isRegist", true);
        ActivityJumpUtils.nextWithData(LoginActivity.this, LoginActivity.class, true, bundle);
    }


    /**
     * 登陆
     */
    @OnClick(R.id.bt_login)
    void login() {
        String username = etUserName.getText().toString();
        String password = etUserPass.getText().toString();
        CommonDialogUtils.showProgressDialog(this);
        AVUser user = new AVUser();// 新建 AVUser 对象实例
        user.setUsername(username);// 设置用户名
        user.setPassword(password);// 设置密码
        if (isRegist) {
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(AVException e) {
                    CommonDialogUtils.dismissProgressDialog();
                    if (e == null) {
                        showToast("注册成功");
                        // 注册成功，把用户对象赋值给当前用户 AVUser.getCurrentUser()
                        ActivityJumpUtils.commonNext(LoginActivity.this, LoginActivity.class, true);
                    } else {
                        // 失败的原因可能有多种，常见的是用户名已经存在。
                        showToast(e.getMessage());
                    }
                }
            });
        }else {
            AVUser.logInInBackground(username, password, new LogInCallback<AVUser>() {
                @Override
                public void done(AVUser avUser, AVException e) {
                    CommonDialogUtils.dismissProgressDialog();
                    if (e == null) {
                        ActivityJumpUtils.commonNext(LoginActivity.this, MainActivity.class, true);
                    } else {
                       showToast(e.getMessage());
                    }
                }
            });
        }
    }

    private void showToast(String message) {
        Toast.makeText(LoginActivity.this,message,Toast.LENGTH_LONG).show();
    }

    /**
     * 全清手机号
     */
    @OnClick(R.id.login_img_delete)
    void clearPhoneNum() {
        etUserName.setText("");
        etUserName.requestFocus();
    }



    /**
     * 小眼睛显示密码明文
     */
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        showPassWordVisible(isChecked);
    }

    /**
     * 监听输入框，手机号的显示删除全部图片。按钮是否可点击
     */
    @OnTextChanged(value = {R.id.login_edt_username, R.id.login_edt_password}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void listenEditText(Editable chars) {
        if (etUserName.getText().toString().length() > 0) {
            imDelete.setVisibility(View.VISIBLE);
        } else {
            imDelete.setVisibility(View.GONE);
        }
        setLoginButtonEnable();
    }



    /**
     * 设置按钮是否可以点击
     */
    public void setLoginButtonEnable() {
        Boolean clickable = !(TextUtils.isEmpty(etUserName.getText().toString()) || TextUtils.isEmpty(etUserPass.getText().toString()));
        btLogin.setEnabled(clickable);
        if (clickable) {
            btLogin.setTextColor(getResources().getColor(R.color.white));
            btLogin.setBackgroundColor(getResources().getColor(R.color.color_blue_btn_release));
        } else {
            btLogin.setTextColor(getResources().getColor(R.color.white_alpha));
            btLogin.setBackgroundColor(getResources().getColor(R.color.colorffb6b5));
        }
    }

    public void showGraphCode(String code) {
        rlVerficationCode.setVisibility(View.VISIBLE);
        codeView.setText(code);
    }


    public void jumpOthersView() {

        showToast(StringUtils.getRes(R.string.login_success));
        ActivityJumpUtils.commonNext(this, MainActivity.class, true);
    }
}
