package ll.com.gaopincaiiao.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.activity.change_login_pwd.ChangeLoginPwdActivity;
import ll.com.gaopincaiiao.activity.login.LoginActivity;
import ll.com.gaopincaiiao.utils.ActivityJumpUtils;

/**
 * Created by liuyong
 * Data: 2018/6/6
 * Github:https://github.com/MrAllRight
 */

public class MineFragment extends Fragment implements View.OnClickListener {
    private TextView tvName;
    private TextView tvPhone;
    private RelativeLayout rlChangePass;
    private Button btLogOut;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mine, container, false);
        tvName= (TextView) view.findViewById(R.id.tv_name);
        tvPhone= (TextView) view.findViewById(R.id.tv_phone);
        btLogOut= (Button) view.findViewById(R.id.bt_logOut);
        rlChangePass= (RelativeLayout) view.findViewById(R.id.rl_modify_pwd);

        btLogOut.setOnClickListener(this);
        rlChangePass.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_logOut:
                ActivityJumpUtils.commonNext(getActivity(), LoginActivity.class,true);
                break;

            case R.id.rl_modify_pwd:
                ActivityJumpUtils.commonNext(getActivity(), ChangeLoginPwdActivity.class,false);
                break;
        }
    }

}
