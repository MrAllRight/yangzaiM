package ll.com.gaopincaiiao.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ll.com.gaopincaiiao.R;
import ll.com.gaopincaiiao.activity.common_webview.JsBridgeWebView;
import ll.com.gaopincaiiao.utils.SaveUtils;

/**
 * Created by liuyong
 * Data: 2018/6/6
 * Github:https://github.com/MrAllRight
 */

public class HomeFragment extends Fragment implements View.OnClickListener {
    JsBridgeWebView webView;
    private Activity activity;
    private View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        activity = getActivity();
        webView = (JsBridgeWebView) view.findViewById(R.id.common_web);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        String url = SaveUtils.getPreferences("url", "file:///android_asset/www/index.html");
        webView.loadUrl(url);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }

    }

    /**
     * 有返回关闭
     */
    private void goBack() {
        if (webView.canGoBack()) {
            webView.goBack();
        }
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            goBack();
        }
        return true;
    }
}
