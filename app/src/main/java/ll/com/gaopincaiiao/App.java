package ll.com.gaopincaiiao;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.widget.Toast;

import com.avos.avoscloud.AVOSCloud;

import java.util.LinkedList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;



/**
 * @author YuliangZhang
 * @create 2017/5/15.
 * @description BankPlatformMvp
 */


public class App extends Application {

    private static App application;
    private static List<Activity> activitys = new LinkedList<Activity>();//页面栈信息

    public static String versionName;//版本名称
    public static int versionCode;//版本code
    public static String deviceID;

    private long exitTime = 0;//两次后退键退出时间

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        // 初始化参数依次为 this, AppId, AppKey
        AVOSCloud.initialize(this,"N2A0NwFacnAo5qUxvqrV0tMB-gzGzoHsz","aX7Lo2OpLiRjDa4iEcsE5oCI");
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);

        initLifecycleCallbacks();
    }


    /**
     * 初始化生命周期回调
     */
    private void initLifecycleCallbacks() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                addActivity(activity);
            }

            @Override
            public void onActivityStarted(Activity activity) {
            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                removeActivity(activity);
            }
        });
    }




    /**
     * 将打开的页面加入到本地容器
     */
    public static Activity addActivity(Activity activity) {
        synchronized (activitys) {
            activitys.add(activity);
            return activity;
        }
    }

    /**
     * 清除容器中的activity
     */
    public void removeActivity(Activity reference) {
        synchronized (activitys) {
            activitys.remove(reference);
        }
    }

    /**
     * 关闭容器中所有activity
     */
    public void exit() {
        synchronized (activitys) {
            //清除所有对话框防止内存泄漏
            //清除栈的页面
            for (Activity activityRef : activitys) {
                Activity activity = activityRef;
                if (activity != null) {
                    activity.finish();
                }
            }
            activitys.clear();
        }
    }

    /**
     * 双击返回退出程序
     */
    public void doubleClickExit(Activity context) {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(context, getString(R.string.app_exit_notify_text),Toast.LENGTH_LONG).show();
            exitTime = System.currentTimeMillis();
        } else {
            context.finish();
            App.getInstance().exit();
            System.exit(0);
        }
    }

    /**
     * 获取Application实例
     */
    public static App getInstance() {
        return application;
    }

}
